﻿namespace HealthCare_Plus__HMS.Doctor
{
    partial class ViewPatientRecords
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbPatName = new System.Windows.Forms.ComboBox();
            this.btnView = new Guna.UI2.WinForms.Guna2Button();
            this.btnUpdate = new Guna.UI2.WinForms.Guna2Button();
            this.tblDoctor = new System.Windows.Forms.DataGridView();
            this.txtDiagnoses = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtPrescription = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMedicalHistory = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.tblDoctor)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbPatName
            // 
            this.cmbPatName.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.cmbPatName.FormattingEnabled = true;
            this.cmbPatName.Items.AddRange(new object[] {
            "Urology",
            "Gynecology",
            "Surgery",
            "Ophtalmo",
            "Dermato",
            "Generalist"});
            this.cmbPatName.Location = new System.Drawing.Point(28, 87);
            this.cmbPatName.Margin = new System.Windows.Forms.Padding(2);
            this.cmbPatName.Name = "cmbPatName";
            this.cmbPatName.Size = new System.Drawing.Size(182, 30);
            this.cmbPatName.TabIndex = 102;
            // 
            // btnView
            // 
            this.btnView.BorderRadius = 10;
            this.btnView.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnView.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnView.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnView.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnView.FillColor = System.Drawing.Color.Gray;
            this.btnView.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnView.ForeColor = System.Drawing.Color.White;
            this.btnView.Location = new System.Drawing.Point(892, 256);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(107, 29);
            this.btnView.TabIndex = 101;
            this.btnView.Text = "View";
            // 
            // btnUpdate
            // 
            this.btnUpdate.BorderRadius = 10;
            this.btnUpdate.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnUpdate.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnUpdate.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnUpdate.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnUpdate.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(189)))), ((int)(((byte)(50)))));
            this.btnUpdate.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnUpdate.ForeColor = System.Drawing.Color.White;
            this.btnUpdate.Location = new System.Drawing.Point(780, 256);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(107, 29);
            this.btnUpdate.TabIndex = 100;
            this.btnUpdate.Text = "Update";
            // 
            // tblDoctor
            // 
            this.tblDoctor.BackgroundColor = System.Drawing.Color.White;
            this.tblDoctor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tblDoctor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tblDoctor.GridColor = System.Drawing.Color.White;
            this.tblDoctor.Location = new System.Drawing.Point(31, 300);
            this.tblDoctor.Margin = new System.Windows.Forms.Padding(2);
            this.tblDoctor.Name = "tblDoctor";
            this.tblDoctor.RowHeadersWidth = 62;
            this.tblDoctor.RowTemplate.Height = 28;
            this.tblDoctor.Size = new System.Drawing.Size(968, 310);
            this.tblDoctor.TabIndex = 98;
            // 
            // txtDiagnoses
            // 
            this.txtDiagnoses.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.txtDiagnoses.ForeColor = System.Drawing.Color.Black;
            this.txtDiagnoses.Location = new System.Drawing.Point(240, 89);
            this.txtDiagnoses.Margin = new System.Windows.Forms.Padding(2);
            this.txtDiagnoses.Multiline = true;
            this.txtDiagnoses.Name = "txtDiagnoses";
            this.txtDiagnoses.Size = new System.Drawing.Size(236, 84);
            this.txtDiagnoses.TabIndex = 95;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(236, 67);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 20);
            this.label2.TabIndex = 94;
            this.label2.Text = "Diagnoses";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label9.Location = new System.Drawing.Point(27, 67);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(108, 20);
            this.label9.TabIndex = 88;
            this.label9.Text = "Patient Name";
            // 
            // txtPrescription
            // 
            this.txtPrescription.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.txtPrescription.ForeColor = System.Drawing.Color.Black;
            this.txtPrescription.Location = new System.Drawing.Point(505, 87);
            this.txtPrescription.Margin = new System.Windows.Forms.Padding(2);
            this.txtPrescription.Multiline = true;
            this.txtPrescription.Name = "txtPrescription";
            this.txtPrescription.Size = new System.Drawing.Size(236, 84);
            this.txtPrescription.TabIndex = 104;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(501, 65);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 20);
            this.label1.TabIndex = 103;
            this.label1.Text = "Prescription";
            // 
            // txtMedicalHistory
            // 
            this.txtMedicalHistory.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.txtMedicalHistory.ForeColor = System.Drawing.Color.Black;
            this.txtMedicalHistory.Location = new System.Drawing.Point(763, 87);
            this.txtMedicalHistory.Margin = new System.Windows.Forms.Padding(2);
            this.txtMedicalHistory.Multiline = true;
            this.txtMedicalHistory.Name = "txtMedicalHistory";
            this.txtMedicalHistory.Size = new System.Drawing.Size(236, 84);
            this.txtMedicalHistory.TabIndex = 106;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(759, 65);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 20);
            this.label3.TabIndex = 105;
            this.label3.Text = "Medical History";
            // 
            // ViewPatientRecords
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1027, 677);
            this.Controls.Add(this.txtMedicalHistory);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtPrescription);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbPatName);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.tblDoctor);
            this.Controls.Add(this.txtDiagnoses);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label9);
            this.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ViewPatientRecords";
            this.Text = "ViewPatientRecords";
            ((System.ComponentModel.ISupportInitialize)(this.tblDoctor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbPatName;
        private Guna.UI2.WinForms.Guna2Button btnView;
        private Guna.UI2.WinForms.Guna2Button btnUpdate;
        private System.Windows.Forms.DataGridView tblDoctor;
        private System.Windows.Forms.TextBox txtDiagnoses;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtPrescription;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtMedicalHistory;
        private System.Windows.Forms.Label label3;
    }
}