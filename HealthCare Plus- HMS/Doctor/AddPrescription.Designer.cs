﻿namespace HealthCare_Plus__HMS.Doctor
{
    partial class AddPrescription
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddPrescription));
            this.label6 = new System.Windows.Forms.Label();
            this.MedTb = new System.Windows.Forms.TextBox();
            this.PrescriptionDGV = new System.Windows.Forms.DataGridView();
            this.PrescSumTxt = new System.Windows.Forms.RichTextBox();
            this.CostTb = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TestIdCb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TestNameTb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.DocIdCb = new System.Windows.Forms.ComboBox();
            this.PrintBtn = new System.Windows.Forms.Button();
            this.AddBtn = new System.Windows.Forms.Button();
            this.PatIdCb = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.PatNameTb = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.DocNameTb = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            ((System.ComponentModel.ISupportInitialize)(this.PrescriptionDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(817, 21);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(156, 26);
            this.label6.TabIndex = 91;
            this.label6.Text = "PRESCRIPTION";
            // 
            // MedTb
            // 
            this.MedTb.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MedTb.ForeColor = System.Drawing.Color.Black;
            this.MedTb.Location = new System.Drawing.Point(269, 299);
            this.MedTb.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.MedTb.Name = "MedTb";
            this.MedTb.Size = new System.Drawing.Size(259, 34);
            this.MedTb.TabIndex = 90;
            // 
            // PrescriptionDGV
            // 
            this.PrescriptionDGV.BackgroundColor = System.Drawing.Color.White;
            this.PrescriptionDGV.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PrescriptionDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PrescriptionDGV.GridColor = System.Drawing.Color.White;
            this.PrescriptionDGV.Location = new System.Drawing.Point(37, 420);
            this.PrescriptionDGV.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.PrescriptionDGV.Name = "PrescriptionDGV";
            this.PrescriptionDGV.RowHeadersWidth = 62;
            this.PrescriptionDGV.RowTemplate.Height = 28;
            this.PrescriptionDGV.Size = new System.Drawing.Size(1088, 278);
            this.PrescriptionDGV.TabIndex = 89;
            this.PrescriptionDGV.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.PrescriptionDGV_CellContentClick);
            // 
            // PrescSumTxt
            // 
            this.PrescSumTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PrescSumTxt.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrescSumTxt.Location = new System.Drawing.Point(651, 64);
            this.PrescSumTxt.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.PrescSumTxt.Name = "PrescSumTxt";
            this.PrescSumTxt.Size = new System.Drawing.Size(474, 336);
            this.PrescSumTxt.TabIndex = 88;
            this.PrescSumTxt.Text = "";
            this.PrescSumTxt.TextChanged += new System.EventHandler(this.PrescSumTxt_TextChanged);
            // 
            // CostTb
            // 
            this.CostTb.Enabled = false;
            this.CostTb.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CostTb.ForeColor = System.Drawing.Color.Black;
            this.CostTb.Location = new System.Drawing.Point(35, 302);
            this.CostTb.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.CostTb.Name = "CostTb";
            this.CostTb.Size = new System.Drawing.Size(175, 34);
            this.CostTb.TabIndex = 87;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(32, 271);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 25);
            this.label5.TabIndex = 86;
            this.label5.Text = "Cost";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(269, 271);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 25);
            this.label4.TabIndex = 85;
            this.label4.Text = "Medicines";
            // 
            // TestIdCb
            // 
            this.TestIdCb.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TestIdCb.FormattingEnabled = true;
            this.TestIdCb.Location = new System.Drawing.Point(35, 220);
            this.TestIdCb.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.TestIdCb.Name = "TestIdCb";
            this.TestIdCb.Size = new System.Drawing.Size(175, 33);
            this.TestIdCb.TabIndex = 84;
            this.TestIdCb.SelectionChangeCommitted += new System.EventHandler(this.TestIdCb_SelectionChangeCommitted);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(32, 191);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 25);
            this.label2.TabIndex = 83;
            this.label2.Text = "Test Id";
            // 
            // TestNameTb
            // 
            this.TestNameTb.Enabled = false;
            this.TestNameTb.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TestNameTb.ForeColor = System.Drawing.Color.Black;
            this.TestNameTb.Location = new System.Drawing.Point(268, 219);
            this.TestNameTb.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.TestNameTb.Name = "TestNameTb";
            this.TestNameTb.Size = new System.Drawing.Size(259, 34);
            this.TestNameTb.TabIndex = 82;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(264, 191);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(127, 25);
            this.label3.TabIndex = 81;
            this.label3.Text = "Test Name";
            // 
            // DocIdCb
            // 
            this.DocIdCb.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DocIdCb.FormattingEnabled = true;
            this.DocIdCb.Location = new System.Drawing.Point(35, 61);
            this.DocIdCb.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.DocIdCb.Name = "DocIdCb";
            this.DocIdCb.Size = new System.Drawing.Size(175, 33);
            this.DocIdCb.TabIndex = 80;
            this.DocIdCb.SelectionChangeCommitted += new System.EventHandler(this.DocIdCb_SelectionChangeCommitted);
            // 
            // PrintBtn
            // 
            this.PrintBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(189)))), ((int)(((byte)(50)))));
            this.PrintBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PrintBtn.FlatAppearance.BorderSize = 10;
            this.PrintBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.PrintBtn.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrintBtn.ForeColor = System.Drawing.Color.White;
            this.PrintBtn.Location = new System.Drawing.Point(489, 362);
            this.PrintBtn.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.PrintBtn.Name = "PrintBtn";
            this.PrintBtn.Size = new System.Drawing.Size(145, 38);
            this.PrintBtn.TabIndex = 79;
            this.PrintBtn.Text = "Print";
            this.PrintBtn.UseVisualStyleBackColor = false;
            this.PrintBtn.Click += new System.EventHandler(this.PrintBtn_Click);
            // 
            // AddBtn
            // 
            this.AddBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(144)))), ((int)(((byte)(234)))));
            this.AddBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddBtn.FlatAppearance.BorderSize = 10;
            this.AddBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.AddBtn.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddBtn.ForeColor = System.Drawing.Color.White;
            this.AddBtn.Location = new System.Drawing.Point(327, 362);
            this.AddBtn.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(145, 38);
            this.AddBtn.TabIndex = 78;
            this.AddBtn.Text = "Add";
            this.AddBtn.UseVisualStyleBackColor = false;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // PatIdCb
            // 
            this.PatIdCb.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PatIdCb.FormattingEnabled = true;
            this.PatIdCb.Location = new System.Drawing.Point(35, 139);
            this.PatIdCb.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.PatIdCb.Name = "PatIdCb";
            this.PatIdCb.Size = new System.Drawing.Size(175, 33);
            this.PatIdCb.TabIndex = 77;
            this.PatIdCb.SelectedIndexChanged += new System.EventHandler(this.PatIdCb_SelectedIndexChanged);
            this.PatIdCb.SelectionChangeCommitted += new System.EventHandler(this.PatIdCb_SelectionChangeCommitted);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(32, 111);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(114, 25);
            this.label12.TabIndex = 76;
            this.label12.Text = "Patient Id";
            // 
            // PatNameTb
            // 
            this.PatNameTb.Enabled = false;
            this.PatNameTb.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PatNameTb.ForeColor = System.Drawing.Color.Black;
            this.PatNameTb.Location = new System.Drawing.Point(268, 138);
            this.PatNameTb.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.PatNameTb.Name = "PatNameTb";
            this.PatNameTb.Size = new System.Drawing.Size(259, 34);
            this.PatNameTb.TabIndex = 75;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(264, 111);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(160, 25);
            this.label11.TabIndex = 74;
            this.label11.Text = "Patient Name";
            // 
            // DocNameTb
            // 
            this.DocNameTb.Enabled = false;
            this.DocNameTb.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DocNameTb.ForeColor = System.Drawing.Color.Black;
            this.DocNameTb.Location = new System.Drawing.Point(268, 60);
            this.DocNameTb.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.DocNameTb.Name = "DocNameTb";
            this.DocNameTb.Size = new System.Drawing.Size(259, 34);
            this.DocNameTb.TabIndex = 73;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(264, 32);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(158, 25);
            this.label9.TabIndex = 72;
            this.label9.Text = "Doctor Name";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(30, 32);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 25);
            this.label8.TabIndex = 71;
            this.label8.Text = "Doctor Id";
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.printDocument1;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // AddPrescription
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1155, 719);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.MedTb);
            this.Controls.Add(this.PrescriptionDGV);
            this.Controls.Add(this.PrescSumTxt);
            this.Controls.Add(this.CostTb);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TestIdCb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TestNameTb);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.DocIdCb);
            this.Controls.Add(this.PrintBtn);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.PatIdCb);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.PatNameTb);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.DocNameTb);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "AddPrescription";
            this.Text = "AddPrescription";
            ((System.ComponentModel.ISupportInitialize)(this.PrescriptionDGV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox MedTb;
        private System.Windows.Forms.DataGridView PrescriptionDGV;
        private System.Windows.Forms.RichTextBox PrescSumTxt;
        private System.Windows.Forms.TextBox CostTb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox TestIdCb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TestNameTb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox DocIdCb;
        private System.Windows.Forms.Button PrintBtn;
        private System.Windows.Forms.Button AddBtn;
        private System.Windows.Forms.ComboBox PatIdCb;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox PatNameTb;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox DocNameTb;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
    }
}