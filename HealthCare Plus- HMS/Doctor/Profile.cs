﻿using FontAwesome.Sharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;

namespace HealthCare_Plus__HMS.Doctor
{
    public partial class Profile : Form
    {

        private string _username;
        private string _password;

        SqlConnection Con = new SqlConnection(@"Data Source=GEETH_MALINDA\SQLEXPRESS;Initial Catalog=HealthCarePlus;Integrated Security=True");

        public Profile(string username, string password)
        {
            InitializeComponent();
            _username = username;
            _password = password;
            LoadDoctorDetails();
        }

        private void LoadDoctorDetails()
        {
            try
            {
                Con.Open();

                // Construct the SQL query
                string query = @"SELECT u.user_id, u.username, u.contact_number, u.email, 
                                s.specialization_name, dp.qualifications
                        FROM Users u
                        INNER JOIN DoctorProfiles dp ON u.user_id = dp.doctor_id
                        INNER JOIN Specializations s ON dp.specialization_id = s.specialization_id
                        WHERE u.username = @Username AND u.password = @Password";

                SqlCommand cmd = new SqlCommand(query, Con);
                cmd.Parameters.AddWithValue("@Username", _username);
                cmd.Parameters.AddWithValue("@Password", _password); // Consider hashing the password

                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    txtDocId.Text = reader["user_id"].ToString();
                    txtDocName.Text = reader["username"].ToString();
                    txtMobnum.Text = reader["contact_number"].ToString();
                    txtEmail.Text = reader["email"].ToString();
                    txtSpecification.Text = reader["specialization_name"].ToString();
                    txtQulifications.Text = reader["qualifications"].ToString();
                }

                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                Con.Close();
            }
        }


        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void txtDocId_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtDocName_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtMobnum_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtSpecification_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtEmail_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtQulifications_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
