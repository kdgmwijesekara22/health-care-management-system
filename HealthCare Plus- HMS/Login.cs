﻿using HealthCare_Plus__HMS.Admin;
using HealthCare_Plus__HMS.Doctor;
using HealthCare_Plus__HMS.Staff;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HealthCare_Plus__HMS
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        SqlConnection Con = new SqlConnection(@"Data Source=GEETH_MALINDA\SQLEXPRESS;Initial Catalog=HealthCarePlus;Integrated Security=True");
        public static string Role;
        private void iconPictureBoxMin_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void iconPictureBoxClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void RoleCb_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Resetlbl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            RoleCb.SelectedIndex = 0;
            UnameTb.Text = "";
            PassTb.Text = "";
        }

        private void LoginBtn_Click(object sender, EventArgs e)
        {
            // Check if username, password, or role is empty
            if (string.IsNullOrEmpty(UnameTb.Text) || string.IsNullOrEmpty(PassTb.Text) || RoleCb.SelectedIndex == -1)
            {
                MessageBox.Show("Please fill in all fields.");
                return;
            }

            // Authenticate the user
            try
            {
                Con.Open();

                string query = "SELECT role FROM Users WHERE username=@username AND password=@password";  // Use parameterized query
                SqlCommand cmd = new SqlCommand(query, Con);
                cmd.Parameters.AddWithValue("@username", UnameTb.Text);
                cmd.Parameters.AddWithValue("@password", PassTb.Text);  // In a real-world application, hash and salt this password

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    string dbRole = reader["role"].ToString();
                    if (dbRole == RoleCb.SelectedItem.ToString())
                    {
                        // Correct Role and Credentials
                        switch (dbRole)
                        {
                            case "Admin":
                                AdminDashboard adminDashboard = new AdminDashboard();
                                adminDashboard.Show();
                                this.Hide();
                                break;
                            case "Doctor":
                                DoctorDashboard doctorDashboard = new DoctorDashboard(UnameTb.Text, PassTb.Text);
                                doctorDashboard.Show();
                                this.Hide();
                                break;
                            // ... Add other roles here
                            default:
                                MessageBox.Show("Invalid Role selected.");
                                break;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Role mismatch. Please select the correct role.");
                    }
                }
                else
                {
                    MessageBox.Show("Invalid Credentials. Please try again.");
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                Con.Close();
            }
        }


        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void UnameTb_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
