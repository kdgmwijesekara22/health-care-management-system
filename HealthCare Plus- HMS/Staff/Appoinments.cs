﻿using PdfSharp.Drawing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
namespace HealthCare_Plus__HMS.Staff
{
    public partial class Appoinments : Form
    {
        public Appoinments()
        {
            InitializeComponent();
            LoadPatientIds();
            LoadDoctorIds(); // Load doctor IDs when the form loads

        }
        SqlConnection Con = new SqlConnection(@"Data Source=GEETH_MALINDA\SQLEXPRESS;Initial Catalog=HealthCarePlus;Integrated Security=True");

        /// <summary>
        /// Loads all doctor IDs from the database into the cmbDoctorId combo box.
        /// </summary>
        private void LoadDoctorIds()
        {
            try
            {
                Con.Open();
                SqlCommand cmd = new SqlCommand("SELECT doctor_id FROM DoctorProfiles", Con);
                SqlDataReader rdr = cmd.ExecuteReader();

                cmbDoctorId.Items.Clear(); // Clear existing items to avoid duplications

                while (rdr.Read())
                {
                    cmbDoctorId.Items.Add(rdr["doctor_id"]);
                }

                rdr.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (Con.State == ConnectionState.Open)
                    Con.Close();
            }
        }


        private void LoadPatientIds()
        {
            try
            {
                Con.Open();
                SqlCommand cmd = new SqlCommand("SELECT patient_id FROM Patients", Con);
                SqlDataReader rdr = cmd.ExecuteReader();

                cmbPatiientId.Items.Clear();  // Clear existing items

                while (rdr.Read())
                {
                    cmbPatiientId.Items.Add(rdr["patient_id"]);
                }

                rdr.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (Con.State == ConnectionState.Open)
                    Con.Close();
            }
        }
        private void cmbPatiientId_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbPatiientId.SelectedItem != null)
            {
                int selectedPatientId = Convert.ToInt32(cmbPatiientId.SelectedItem.ToString());

                try
                {
                    Con.Open();
                    SqlCommand cmd = new SqlCommand("SELECT first_name, last_name, contact_number FROM Patients WHERE patient_id = @PatientId", Con);
                    cmd.Parameters.AddWithValue("@PatientId", selectedPatientId);

                    SqlDataReader rdr = cmd.ExecuteReader();

                    if (rdr.Read())
                    {
                        txtFirstName.Text = rdr["first_name"].ToString();
                        txtLastName.Text = rdr["last_name"].ToString();
                        txtContactNum.Text = rdr["contact_number"].ToString();
                    }

                    rdr.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    if (Con.State == ConnectionState.Open)
                        Con.Close();
                }
            }
        }


        private void txtFirstName_TextChanged(object sender, EventArgs e)
        {

        }

        private void Appoinments_Load(object sender, EventArgs e)
        {

        }

        private void txtLastName_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtContactNum_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Fetches and displays the details (name, specification, room number) of the selected doctor
        /// in the respective fields when the selected item in the cmbDoctorId combo box is changed.
        /// </summary>
        private void cmbDoctorId_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbDoctorId.SelectedItem != null)
            {
                int selectedDoctorId = Convert.ToInt32(cmbDoctorId.SelectedItem.ToString());

                try
                {
                    Con.Open();

                    // Fetching the doctor details
                    SqlCommand cmd = new SqlCommand(@"SELECT U.username, S.specialization_name, D.location 
                                              FROM DoctorProfiles D
                                              JOIN Users U ON D.doctor_id = U.user_id
                                              JOIN Specializations S ON D.specialization_id = S.specialization_id
                                              WHERE D.doctor_id = @DoctorId", Con);
                    cmd.Parameters.AddWithValue("@DoctorId", selectedDoctorId);

                    SqlDataReader rdr = cmd.ExecuteReader();

                    if (rdr.Read())
                    {
                        cmbDoctorName.Text = rdr["username"].ToString();
                        cmbSpecification.Text = rdr["specialization_name"].ToString();
                        txtRoomNo.Text = rdr["location"].ToString();
                    }

                    rdr.Close();

                    // Fetching the doctor availability
                    cmd = new SqlCommand(@"SELECT day_of_week, CONVERT(VARCHAR, start_time, 0) AS start_time, 
                                   CONVERT(VARCHAR, end_time, 0) AS end_time 
                                   FROM DoctorAvailability
                                   WHERE doctor_id = @DoctorId", Con);
                    cmd.Parameters.AddWithValue("@DoctorId", selectedDoctorId);

                    rdr = cmd.ExecuteReader();

                    cmbDoctorAvalability.Items.Clear(); // Clear existing items

                    while (rdr.Read())
                    {
                        string availability = $"{rdr["day_of_week"]} {rdr["start_time"]} - {rdr["end_time"]}";
                        cmbDoctorAvalability.Items.Add(availability);
                    }

                    rdr.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    if (Con.State == ConnectionState.Open)
                        Con.Close();
                }
            }
        }

        private void cmbDoctorName_TextChanged(object sender, EventArgs e)
        {

        }

        private void cmbSpecification_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtRoomNo_TextChanged(object sender, EventArgs e)
        {

        }

        private void cmbDoctorAvalability_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void datePicker_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtHosCharge_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtDoctorCharge_TextChanged(object sender, EventArgs e)
        {

        }

        private void printBill_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnPrintBill_Click(object sender, EventArgs e)
        {
            try
            {
                decimal hospitalCharge = decimal.Parse(txtHosCharge.Text);
                decimal doctorCharge = decimal.Parse(txtDoctorCharge.Text);
                decimal totalAmount = hospitalCharge + doctorCharge;

                string bill = "********************** HealthCare Plus - Bill **********************\n";
                bill += "Patient Name: " + txtFirstName.Text + " " + txtLastName.Text + "\n";
                bill += "Patient ID: " + cmbPatiientId.SelectedItem.ToString() + "\n";
                bill += "Contact: " + txtContactNum.Text + "\n\n";

                bill += "Doctor ID: " + cmbDoctorId.SelectedItem.ToString() + "\n";
                bill += "Doctor Name: " + cmbDoctorName.Text + "\n";
                bill += "Specialization: " + cmbSpecification.Text + "\n\n";

                bill += "Room No: " + txtRoomNo.Text + "\n\n";

                bill += "Hospital Charges: " + txtHosCharge.Text + "\n";
                bill += "Doctor Charges: " + txtDoctorCharge.Text + "\n";
                bill += "-------------------------------------------------------------\n";
                bill += "Total Amount: " + totalAmount.ToString("F2") + "\n";
                bill += "***************************************************************\n";

                // Display bill (replace txtBill with the name of your TextBox/RichTextBox where you want to display the bill)
                printBill.Text = bill;
            }
            catch
            {
                // Handle invalid number format here, if necessary
                MessageBox.Show("Invalid number format in charges.");
            }
        }

        private void btnSaveData_Click(object sender, EventArgs e)
        {
            try
            {
                // Step 1: Validate inputs
                if (cmbPatiientId.SelectedItem == null)
                {
                    MessageBox.Show("Please select a patient.");
                    return;
                }

                if (cmbDoctorId.SelectedItem == null)
                {
                    MessageBox.Show("Please select a doctor.");
                    return;
                }

                if (string.IsNullOrWhiteSpace(txtHosCharge.Text) || !decimal.TryParse(txtHosCharge.Text, out decimal hospitalCharge))
                {
                    MessageBox.Show("Please enter a valid hospital charge.");
                    return;
                }

                if (string.IsNullOrWhiteSpace(txtDoctorCharge.Text) || !decimal.TryParse(txtDoctorCharge.Text, out decimal doctorCharge))
                {
                    MessageBox.Show("Please enter a valid doctor charge.");
                    return;
                }

                if (datePicker.Value.Date < DateTime.Now.Date)
                {
                    MessageBox.Show("Appointment date cannot be in the past.");
                    return;
                }

                // Step 2: Insert appointment details
                if (Con.State == ConnectionState.Closed)
                    Con.Open();

                using (SqlCommand cmd = new SqlCommand(@"INSERT INTO Appointments(patient_id, doctor_id, date_time, status, notes) 
                                  VALUES(@PatientId, @DoctorId, @DateTime, 'Scheduled', @Notes); 
                                  SELECT SCOPE_IDENTITY()", Con))
                {
                    cmd.Parameters.AddWithValue("@PatientId", Convert.ToInt32(cmbPatiientId.SelectedItem));
                    cmd.Parameters.AddWithValue("@DoctorId", Convert.ToInt32(cmbDoctorId.SelectedItem));
                    cmd.Parameters.AddWithValue("@DateTime", datePicker.Value);
                    cmd.Parameters.AddWithValue("@Notes", "");
                    int appointmentId = Convert.ToInt32(cmd.ExecuteScalar());

                    // Step 3: Insert billing details
                    cmd.CommandText = @"INSERT INTO Bills(total_amount, hospital_charges, doctor_charges, appointment_id) 
                               VALUES(@TotalAmount, @HospitalCharges, @DoctorCharges, @AppointmentId)";
                    cmd.Parameters.AddWithValue("@TotalAmount", hospitalCharge + doctorCharge);
                    cmd.Parameters.AddWithValue("@HospitalCharges", hospitalCharge);
                    cmd.Parameters.AddWithValue("@DoctorCharges", doctorCharge);
                    cmd.Parameters.AddWithValue("@AppointmentId", appointmentId);
                    cmd.ExecuteNonQuery();

                    // Step 4: Generate PDF
                    GeneratePDF(appointmentId);

                    MessageBox.Show("Data Saved and PDF Generated Successfully");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (Con.State == ConnectionState.Open)
                    Con.Close();
            }
        }

        private void GeneratePDF(int appointmentId)
        {
            try
            {
                if (Con.State == ConnectionState.Closed)
                    Con.Open();

                // Step 1: Fetch data from the database using the appointmentId parameter
                SqlCommand cmd = new SqlCommand(@"SELECT 
                            A.appointment_id, A.date_time, A.status, A.notes, 
                            B.total_amount, B.hospital_charges, B.doctor_charges, B.date, 
                            P.patient_id, P.first_name as patient_first_name, P.last_name as patient_last_name, P.contact_number as patient_contact, 
                            D.doctor_id, U.username as doctor_name, S.specialization_name as doctor_specialization, 
                            R.room_number 
                          FROM 
                            Appointments A 
                            JOIN Bills B ON A.appointment_id = B.appointment_id 
                            JOIN Patients P ON A.patient_id = P.patient_id 
                            JOIN DoctorProfiles D ON A.doctor_id = D.doctor_id 
                            JOIN Users U ON D.doctor_id = U.user_id 
                            JOIN Specializations S ON D.specialization_id = S.specialization_id 
                            LEFT JOIN Rooms R ON P.patient_id = R.patient_id 
                          WHERE 
                            A.appointment_id = @AppointmentId", Con);

                cmd.Parameters.AddWithValue("@AppointmentId", appointmentId);
                SqlDataReader rdr = cmd.ExecuteReader();

                if (!rdr.Read())
                {
                    throw new Exception("No data found for the given appointment ID.");
                }

                // Step 2: Set up the PDF document, graphics, and fonts
                PdfDocument document = new PdfDocument();
                PdfPage page = document.AddPage();
                XGraphics gfx = XGraphics.FromPdfPage(page);
                XFont headerFont = new XFont("Verdana", 20, XFontStyle.Bold);
                XFont subHeaderFont = new XFont("Verdana", 16, XFontStyle.Regular);
                XFont bodyFont = new XFont("Verdana", 12, XFontStyle.Regular);
                XFont boldFont = new XFont("Verdana", 12, XFontStyle.Bold);

                // Define some formatting
                double lineSpacing = 30;
                double currentY = 20;

                // Step 3: Add header to the PDF
                gfx.DrawString("HealthCare Plus - Appointment Detail", headerFont, XBrushes.Black, new XRect(0, currentY, page.Width, 0), XStringFormats.Center);
                currentY += lineSpacing + 10;

                // Step 4: Add body containing appointment and billing details
                gfx.DrawString("Patient Details", subHeaderFont, XBrushes.Black, new XRect(40, currentY, 0, 0));
                currentY += lineSpacing;

                // Adding patient details
                gfx.DrawString("Patient ID: " + rdr["patient_id"], bodyFont, XBrushes.Black, new XRect(40, currentY, 0, 0));
                currentY += lineSpacing;
                gfx.DrawString("Patient Name: " + rdr["patient_first_name"] + " " + rdr["patient_last_name"], bodyFont, XBrushes.Black, new XRect(40, currentY, 0, 0));
                currentY += lineSpacing;
                gfx.DrawString("Contact: " + rdr["patient_contact"], bodyFont, XBrushes.Black, new XRect(40, currentY, 0, 0));
                currentY += lineSpacing;
                gfx.DrawString("Room No: " + (rdr["room_number"] == DBNull.Value ? "N/A" : rdr["room_number"]), bodyFont, XBrushes.Black, new XRect(40, currentY, 0, 0));
                currentY += lineSpacing;

                // Drawing a line for separation
                XPen linePen = new XPen(XColors.Black, 2);
                gfx.DrawLine(linePen, 40, currentY + 5, page.Width - 40, currentY + 5);
                currentY += lineSpacing;

                gfx.DrawString("Doctor Details", subHeaderFont, XBrushes.Black, new XRect(40, currentY, 0, 0));
                currentY += lineSpacing;

                // Adding doctor details
                gfx.DrawString("Doctor ID: " + rdr["doctor_id"], bodyFont, XBrushes.Black, new XRect(40, currentY, 0, 0));
                currentY += lineSpacing;
                gfx.DrawString("Doctor Name: " + rdr["doctor_name"], bodyFont, XBrushes.Black, new XRect(40, currentY, 0, 0));
                currentY += lineSpacing;
                gfx.DrawString("Specialization: " + rdr["doctor_specialization"], bodyFont, XBrushes.Black, new XRect(40, currentY, 0, 0));
                currentY += lineSpacing;

                // Drawing a line for separation
                gfx.DrawLine(linePen, 40, currentY + 5, page.Width - 40, currentY + 5);
                currentY += lineSpacing;

                // Adding billing details
                gfx.DrawString("Billing Details", subHeaderFont, XBrushes.Black, new XRect(40, currentY, 0, 0));
                currentY += lineSpacing;

                gfx.DrawString("Total Amount: " + rdr["total_amount"], bodyFont, XBrushes.Black, new XRect(40, currentY, 0, 0));
                currentY += lineSpacing;
                gfx.DrawString("Hospital Charges: " + rdr["hospital_charges"], bodyFont, XBrushes.Black, new XRect(40, currentY, 0, 0));
                currentY += lineSpacing;
                gfx.DrawString("Doctor Charges: " + rdr["doctor_charges"], bodyFont, XBrushes.Black, new XRect(40, currentY, 0, 0));
                currentY += lineSpacing;

                // Drawing a line for separation
                gfx.DrawLine(linePen, 40, currentY + 5, page.Width - 40, currentY + 5);
                currentY += lineSpacing;

                // Adding appointment details
                gfx.DrawString("Appointment Details", subHeaderFont, XBrushes.Black, new XRect(40, currentY, 0, 0));
                currentY += lineSpacing;

                gfx.DrawString("Appointment ID: " + rdr["appointment_id"], bodyFont, XBrushes.Black, new XRect(40, currentY, 0, 0));
                currentY += lineSpacing;
                gfx.DrawString("Appointment Date: " + ((DateTime)rdr["date_time"]).ToString("d"), bodyFont, XBrushes.Black, new XRect(40, currentY, 0, 0));
                currentY += lineSpacing;

                // Step 5: Add footer
                currentY = page.Height - 50;
                gfx.DrawString("Thank you for choosing HealthCare Plus!", subHeaderFont, XBrushes.Black, new XRect(0, currentY, page.Width, 0), XStringFormats.Center);

                // Step 6: Save the PDF and notify the user
                string pdfPath = "D:\\AD Projects\\pdf\\Appointment.pdf";
                document.Save(pdfPath);

                MessageBox.Show("PDF generated successfully!\nFile saved at: " + pdfPath);
                rdr.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (Con.State == ConnectionState.Open)
                    Con.Close();
            }
        }




    }
}
