﻿namespace HealthCare_Plus__HMS.Staff
{
    partial class Appoinments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Appoinments));
            this.btnPrintBill = new Guna.UI2.WinForms.Guna2Button();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbPatiientId = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnSaveData = new Guna.UI2.WinForms.Guna2Button();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtContactNum = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRoomNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbSpecification = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbDoctorName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbDoctorId = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.datePicker = new System.Windows.Forms.DateTimePicker();
            this.printBill = new System.Windows.Forms.RichTextBox();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.txtHosCharge = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDoctorCharge = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.cmbDoctorAvalability = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btnPrintBill
            // 
            this.btnPrintBill.BorderRadius = 10;
            this.btnPrintBill.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnPrintBill.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnPrintBill.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnPrintBill.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnPrintBill.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(189)))), ((int)(((byte)(50)))));
            this.btnPrintBill.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnPrintBill.ForeColor = System.Drawing.Color.White;
            this.btnPrintBill.Location = new System.Drawing.Point(32, 450);
            this.btnPrintBill.Margin = new System.Windows.Forms.Padding(2);
            this.btnPrintBill.Name = "btnPrintBill";
            this.btnPrintBill.Size = new System.Drawing.Size(146, 36);
            this.btnPrintBill.TabIndex = 145;
            this.btnPrintBill.Text = "Generate";
            this.btnPrintBill.Click += new System.EventHandler(this.btnPrintBill_Click);
            // 
            // txtFirstName
            // 
            this.txtFirstName.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirstName.ForeColor = System.Drawing.Color.Black;
            this.txtFirstName.Location = new System.Drawing.Point(219, 39);
            this.txtFirstName.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(172, 23);
            this.txtFirstName.TabIndex = 141;
            this.txtFirstName.TextChanged += new System.EventHandler(this.txtFirstName_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(215, 21);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 17);
            this.label2.TabIndex = 140;
            this.label2.Text = "First Name";
            // 
            // cmbPatiientId
            // 
            this.cmbPatiientId.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPatiientId.FormattingEnabled = true;
            this.cmbPatiientId.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.cmbPatiientId.Location = new System.Drawing.Point(29, 38);
            this.cmbPatiientId.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmbPatiientId.Name = "cmbPatiientId";
            this.cmbPatiientId.Size = new System.Drawing.Size(176, 25);
            this.cmbPatiientId.TabIndex = 135;
            this.cmbPatiientId.SelectedIndexChanged += new System.EventHandler(this.cmbPatiientId_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(26, 18);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 17);
            this.label12.TabIndex = 134;
            this.label12.Text = "Patient ID";
            // 
            // btnSaveData
            // 
            this.btnSaveData.BorderRadius = 10;
            this.btnSaveData.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnSaveData.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnSaveData.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnSaveData.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnSaveData.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(144)))), ((int)(((byte)(234)))));
            this.btnSaveData.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnSaveData.ForeColor = System.Drawing.Color.White;
            this.btnSaveData.Location = new System.Drawing.Point(192, 450);
            this.btnSaveData.Margin = new System.Windows.Forms.Padding(2);
            this.btnSaveData.Name = "btnSaveData";
            this.btnSaveData.Size = new System.Drawing.Size(154, 36);
            this.btnSaveData.TabIndex = 128;
            this.btnSaveData.Text = "Print";
            this.btnSaveData.Click += new System.EventHandler(this.btnSaveData_Click);
            // 
            // txtLastName
            // 
            this.txtLastName.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLastName.ForeColor = System.Drawing.Color.Black;
            this.txtLastName.Location = new System.Drawing.Point(405, 39);
            this.txtLastName.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(172, 23);
            this.txtLastName.TabIndex = 147;
            this.txtLastName.TextChanged += new System.EventHandler(this.txtLastName_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(401, 21);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 17);
            this.label1.TabIndex = 146;
            this.label1.Text = "Last Name";
            // 
            // txtContactNum
            // 
            this.txtContactNum.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContactNum.ForeColor = System.Drawing.Color.Black;
            this.txtContactNum.Location = new System.Drawing.Point(595, 39);
            this.txtContactNum.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtContactNum.Name = "txtContactNum";
            this.txtContactNum.Size = new System.Drawing.Size(172, 23);
            this.txtContactNum.TabIndex = 149;
            this.txtContactNum.TextChanged += new System.EventHandler(this.txtContactNum_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(591, 21);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 17);
            this.label4.TabIndex = 148;
            this.label4.Text = "Contact";
            // 
            // txtRoomNo
            // 
            this.txtRoomNo.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRoomNo.ForeColor = System.Drawing.Color.Black;
            this.txtRoomNo.Location = new System.Drawing.Point(596, 101);
            this.txtRoomNo.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtRoomNo.Name = "txtRoomNo";
            this.txtRoomNo.Size = new System.Drawing.Size(172, 23);
            this.txtRoomNo.TabIndex = 157;
            this.txtRoomNo.TextChanged += new System.EventHandler(this.txtRoomNo_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(592, 83);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 17);
            this.label3.TabIndex = 156;
            this.label3.Text = "Room No";
            // 
            // cmbSpecification
            // 
            this.cmbSpecification.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSpecification.ForeColor = System.Drawing.Color.Black;
            this.cmbSpecification.Location = new System.Drawing.Point(406, 101);
            this.cmbSpecification.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmbSpecification.Name = "cmbSpecification";
            this.cmbSpecification.Size = new System.Drawing.Size(172, 23);
            this.cmbSpecification.TabIndex = 155;
            this.cmbSpecification.TextChanged += new System.EventHandler(this.cmbSpecification_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(402, 83);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 17);
            this.label5.TabIndex = 154;
            this.label5.Text = "Specification";
            // 
            // cmbDoctorName
            // 
            this.cmbDoctorName.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDoctorName.ForeColor = System.Drawing.Color.Black;
            this.cmbDoctorName.Location = new System.Drawing.Point(220, 101);
            this.cmbDoctorName.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmbDoctorName.Name = "cmbDoctorName";
            this.cmbDoctorName.Size = new System.Drawing.Size(172, 23);
            this.cmbDoctorName.TabIndex = 153;
            this.cmbDoctorName.TextChanged += new System.EventHandler(this.cmbDoctorName_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(216, 83);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 17);
            this.label6.TabIndex = 152;
            this.label6.Text = "Doctor Name";
            // 
            // cmbDoctorId
            // 
            this.cmbDoctorId.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDoctorId.FormattingEnabled = true;
            this.cmbDoctorId.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.cmbDoctorId.Location = new System.Drawing.Point(30, 100);
            this.cmbDoctorId.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmbDoctorId.Name = "cmbDoctorId";
            this.cmbDoctorId.Size = new System.Drawing.Size(176, 25);
            this.cmbDoctorId.TabIndex = 151;
            this.cmbDoctorId.SelectedIndexChanged += new System.EventHandler(this.cmbDoctorId_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(27, 80);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 17);
            this.label7.TabIndex = 150;
            this.label7.Text = "Doctor ID";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(30, 208);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 17);
            this.label13.TabIndex = 159;
            this.label13.Text = "Date";
            // 
            // datePicker
            // 
            this.datePicker.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datePicker.Location = new System.Drawing.Point(32, 228);
            this.datePicker.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.datePicker.Name = "datePicker";
            this.datePicker.Size = new System.Drawing.Size(315, 23);
            this.datePicker.TabIndex = 158;
            this.datePicker.ValueChanged += new System.EventHandler(this.datePicker_ValueChanged);
            // 
            // printBill
            // 
            this.printBill.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.printBill.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.printBill.Location = new System.Drawing.Point(363, 162);
            this.printBill.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.printBill.Name = "printBill";
            this.printBill.Size = new System.Drawing.Size(414, 324);
            this.printBill.TabIndex = 163;
            this.printBill.Text = "";
            this.printBill.TextChanged += new System.EventHandler(this.printBill_TextChanged);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.printDocument1;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // txtHosCharge
            // 
            this.txtHosCharge.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHosCharge.ForeColor = System.Drawing.Color.Black;
            this.txtHosCharge.Location = new System.Drawing.Point(32, 302);
            this.txtHosCharge.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtHosCharge.Name = "txtHosCharge";
            this.txtHosCharge.Size = new System.Drawing.Size(315, 23);
            this.txtHosCharge.TabIndex = 168;
            this.txtHosCharge.TextChanged += new System.EventHandler(this.txtHosCharge_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(30, 282);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 17);
            this.label9.TabIndex = 167;
            this.label9.Text = "Hospital Charge";
            // 
            // txtDoctorCharge
            // 
            this.txtDoctorCharge.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDoctorCharge.ForeColor = System.Drawing.Color.Black;
            this.txtDoctorCharge.Location = new System.Drawing.Point(33, 368);
            this.txtDoctorCharge.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtDoctorCharge.Name = "txtDoctorCharge";
            this.txtDoctorCharge.Size = new System.Drawing.Size(314, 23);
            this.txtDoctorCharge.TabIndex = 170;
            this.txtDoctorCharge.TextChanged += new System.EventHandler(this.txtDoctorCharge_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(30, 348);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(106, 17);
            this.label10.TabIndex = 169;
            this.label10.Text = "Doctor Charge";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(29, 142);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(124, 17);
            this.label14.TabIndex = 171;
            this.label14.Text = "Doctor avalability";
            // 
            // cmbDoctorAvalability
            // 
            this.cmbDoctorAvalability.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDoctorAvalability.FormattingEnabled = true;
            this.cmbDoctorAvalability.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.cmbDoctorAvalability.Location = new System.Drawing.Point(32, 162);
            this.cmbDoctorAvalability.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmbDoctorAvalability.Name = "cmbDoctorAvalability";
            this.cmbDoctorAvalability.Size = new System.Drawing.Size(314, 25);
            this.cmbDoctorAvalability.TabIndex = 177;
            this.cmbDoctorAvalability.SelectedIndexChanged += new System.EventHandler(this.cmbDoctorAvalability_SelectedIndexChanged);
            // 
            // Appoinments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(818, 589);
            this.Controls.Add(this.cmbDoctorAvalability);
            this.Controls.Add(this.txtDoctorCharge);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtHosCharge);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.printBill);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.datePicker);
            this.Controls.Add(this.txtRoomNo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmbSpecification);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cmbDoctorName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cmbDoctorId);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtContactNum);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtLastName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtFirstName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbPatiientId);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.btnPrintBill);
            this.Controls.Add(this.btnSaveData);
            this.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Appoinments";
            this.Text = "Appoinments";
            this.Load += new System.EventHandler(this.Appoinments_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Button btnPrintBill;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbPatiientId;
        private System.Windows.Forms.Label label12;
        private Guna.UI2.WinForms.Guna2Button btnSaveData;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtContactNum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtRoomNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox cmbSpecification;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox cmbDoctorName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbDoctorId;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker datePicker;
        private System.Windows.Forms.RichTextBox printBill;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.TextBox txtHosCharge;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtDoctorCharge;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cmbDoctorAvalability;
    }
}