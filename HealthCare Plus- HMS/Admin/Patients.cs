﻿using iText.Kernel.Pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using System.Windows.Documents;


namespace HealthCare_Plus__HMS.Admin
{
    public partial class Patients : Form
    {
        public Patients()
        {
            InitializeComponent();
            LoadPatients();

        }


        SqlConnection Con = new SqlConnection(@"Data Source=GEETH_MALINDA\SQLEXPRESS;Initial Catalog=HealthCarePlus;Integrated Security=True");
        private int SelectedPatientId = 0; // Default value

        private void LoadPatients()
        {
            try
            {
                if (Con.State == ConnectionState.Closed)
                    Con.Open();

                // Fetch all patient records
                string Query = "SELECT * FROM Patients";
                SqlDataAdapter sda = new SqlDataAdapter(Query, Con);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                tbl.DataSource = dt;  // Assuming 'tbl' is the name of your DataGridView
                tbl.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                if (Con.State == ConnectionState.Open)
                    Con.Close();
            }
        }

        private void ClearAllFields()
        {
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtDob.Value = DateTime.Now; 
            txtGender.SelectedIndex = -1; 
            txtContactNum.Text = "";
            txtAddress.Text = "";
            txtMedHistory.Text = "";
        }


      


        private void Patients_Load(object sender, EventArgs e)
        {

        }

        private void txtFirstName_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtLastName_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtDob_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtGender_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtContactNum_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtAddress_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtMedHistory_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //  Ensure necessary fields are not empty
            if (string.IsNullOrWhiteSpace(txtFirstName.Text) || string.IsNullOrWhiteSpace(txtLastName.Text))
            {
                MessageBox.Show("First Name and Last Name are mandatory.");
                return;
            }

            // Check for valid date of birth (optional: you might want to ensure the patient is above a certain age, for example)
            if (txtDob.Value.Date > DateTime.Now.Date)
            {
                MessageBox.Show("Date of Birth cannot be in the future.");
                return;
            }

            // Check for valid gender
            if (txtGender.SelectedItem == null ||
                (txtGender.SelectedItem.ToString() != "Male" && txtGender.SelectedItem.ToString() != "Female" && txtGender.SelectedItem.ToString() != "Other"))
            {
                MessageBox.Show("Please select a valid gender.");
                return;
            }

            // Check for valid contact number (optional: regex might be used for more detailed validation)
            if (txtContactNum.Text.Length != 10) // Assuming a 10-digit phone number
            {
                MessageBox.Show("Contact Number should be 10 digits.");
                return;
            }


            try
            {
                if (Con.State == ConnectionState.Closed)
                    Con.Open();

                // Construct the SQL command to insert a new patient record
                SqlCommand cmd = new SqlCommand("INSERT INTO Patients(first_name, last_name, date_of_birth, gender, contact_number, address, medical_history) VALUES(@FirstName, @LastName, @DOB, @Gender, @ContactNum, @Address, @MedHistory)", Con);
                cmd.Parameters.AddWithValue("@FirstName", txtFirstName.Text);
                cmd.Parameters.AddWithValue("@LastName", txtLastName.Text);
                cmd.Parameters.AddWithValue("@DOB", txtDob.Value); // Assuming txtDob is a DateTimePicker
                cmd.Parameters.AddWithValue("@Gender", txtGender.SelectedItem.ToString()); // Assuming txtGender is a ComboBox
                cmd.Parameters.AddWithValue("@ContactNum", txtContactNum.Text);
                cmd.Parameters.AddWithValue("@Address", txtAddress.Text);
                cmd.Parameters.AddWithValue("@MedHistory", txtMedHistory.Text);

                cmd.ExecuteNonQuery();

                MessageBox.Show("Patient Added Successfully");
                LoadPatients(); // Refresh the patient table after adding
                ClearAllFields(); // Clear all input fields after adding
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                if (Con.State == ConnectionState.Open)
                    Con.Close();
            }
        }


        private void btnUpdate_Click(object sender, EventArgs e)
        {
            // Check if a record is selected for update 
            if (SelectedPatientId == 0) 
            {
                MessageBox.Show("Please select a patient record to update.");
                return;
            }

            // Basic validation: Ensure necessary fields are not empty
            if (string.IsNullOrWhiteSpace(txtFirstName.Text) || string.IsNullOrWhiteSpace(txtLastName.Text))
            {
                MessageBox.Show("First Name and Last Name are mandatory.");
                return;
            }

            // Check for valid date of birth (optional: you might want to ensure the patient is above a certain age, for example)
            if (txtDob.Value.Date > DateTime.Now.Date)
            {
                MessageBox.Show("Date of Birth cannot be in the future.");
                return;
            }

            // Check for valid gender
            if (txtGender.SelectedItem == null ||
                (txtGender.SelectedItem.ToString() != "Male" && txtGender.SelectedItem.ToString() != "Female" && txtGender.SelectedItem.ToString() != "Other"))
            {
                MessageBox.Show("Please select a valid gender.");
                return;
            }

            // Check for valid contact number 
            if (txtContactNum.Text.Length != 10) // Assuming a 10-digit phone number
            {
                MessageBox.Show("Contact Number should be 10 digits.");
                return;
            }

        

            try
            {
                if (Con.State == ConnectionState.Closed)
                    Con.Open();

                // SQL command to update the selected patient record
                SqlCommand cmd = new SqlCommand("UPDATE Patients SET first_name=@FirstName, last_name=@LastName, date_of_birth=@DOB, gender=@Gender, contact_number=@ContactNum, address=@Address, medical_history=@MedHistory WHERE patient_id=@PatientId", Con);
                cmd.Parameters.AddWithValue("@FirstName", txtFirstName.Text);
                cmd.Parameters.AddWithValue("@LastName", txtLastName.Text);
                cmd.Parameters.AddWithValue("@DOB", txtDob.Value);
                cmd.Parameters.AddWithValue("@Gender", txtGender.SelectedItem.ToString());
                cmd.Parameters.AddWithValue("@ContactNum", txtContactNum.Text);
                cmd.Parameters.AddWithValue("@Address", txtAddress.Text);
                cmd.Parameters.AddWithValue("@MedHistory", txtMedHistory.Text);
                cmd.Parameters.AddWithValue("@PatientId", SelectedPatientId);  // Use the correct variable that holds the selected patient's ID

                cmd.ExecuteNonQuery();

                MessageBox.Show("Patient Updated Successfully");
                LoadPatients(); // Refresh the patient table after updating
                ClearAllFields(); // Clear all input fields after updating
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                if (Con.State == ConnectionState.Open)
                    Con.Close();
            }
        }

        private void tbl_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            // Ensure the click wasn't on a header row
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = tbl.Rows[e.RowIndex];


                // storing the selected patient's ID in a variable named 'SelectedPatientId'
                SelectedPatientId = Convert.ToInt32(row.Cells["patient_id"].Value); // Adjust the column name based on your actual column name for patient_id

                // Load the clicked patient's data into the form's fields
                txtFirstName.Text = row.Cells["first_name"].Value.ToString();
                txtLastName.Text = row.Cells["last_name"].Value.ToString();
                txtDob.Value = Convert.ToDateTime(row.Cells["date_of_birth"].Value);
                txtGender.SelectedItem = row.Cells["gender"].Value.ToString();
                txtContactNum.Text = row.Cells["contact_number"].Value.ToString();
                txtAddress.Text = row.Cells["address"].Value.ToString();
                txtMedHistory.Text = row.Cells["medical_history"].Value.ToString();

                // If there are other fields in your DataGridView, continue this pattern to populate them in the form.
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            // Check if a record is selected for deletion 
            if (SelectedPatientId == 0) // Or whatever default uninitialized value you use
            {
                MessageBox.Show("Please select a patient record to delete.");
                return;
            }

            // Ask the user for confirmation before deletion
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to delete this patient?", "Delete Confirmation", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
            {
                return;  // User chose not to delete, exit the method
            }

            try
            {
                if (Con.State == ConnectionState.Closed)
                    Con.Open();

                // SQL command to delete the selected patient record
                SqlCommand cmd = new SqlCommand("DELETE FROM Patients WHERE patient_id=@PatientId", Con);
                cmd.Parameters.AddWithValue("@PatientId", SelectedPatientId);

                cmd.ExecuteNonQuery();

                MessageBox.Show("Patient Deleted Successfully");
                LoadPatients(); // Refresh the patient table after deleting
                ClearAllFields(); // Clear all input fields after deleting
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                if (Con.State == ConnectionState.Open)
                    Con.Close();
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (Con.State == ConnectionState.Closed)
                    Con.Open();

                // Using the LIKE query to filter patient records based on the entered text
                string Query = "SELECT * FROM Patients WHERE first_name LIKE @SearchText OR last_name LIKE @SearchText OR contact_number LIKE @SearchText";
                // You can add more columns to the WHERE clause if needed

                SqlDataAdapter sda = new SqlDataAdapter(Query, Con);
                sda.SelectCommand.Parameters.AddWithValue("@SearchText", "%" + txtSearch.Text + "%"); // Using '%' to get partial matches
                DataTable dt = new DataTable();
                sda.Fill(dt);

                tbl.DataSource = dt;  // Refreshing the DataGridView with the filtered results
                tbl.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                if (Con.State == ConnectionState.Open)
                    Con.Close();
            }
        }

        private void btnGeneratePdf_Click(object sender, EventArgs e)
        {
            // Create a new PDF document
            var document = new PdfSharp.Pdf.PdfDocument();
            var page = document.AddPage();
            var gfx = PdfSharp.Drawing.XGraphics.FromPdfPage(page);

            // Define fonts
            var headerFont = new PdfSharp.Drawing.XFont("Verdana", 20, PdfSharp.Drawing.XFontStyle.Bold);
            var bodyFont = new PdfSharp.Drawing.XFont("Verdana", 16, PdfSharp.Drawing.XFontStyle.Regular);
            var bodyFontBold = new PdfSharp.Drawing.XFont("Verdana", 16, PdfSharp.Drawing.XFontStyle.Bold); // Bold font for key values
            var footerFont = new PdfSharp.Drawing.XFont("Verdana", 16, PdfSharp.Drawing.XFontStyle.Italic);

            // Define colors
            var headerColor = PdfSharp.Drawing.XBrushes.SteelBlue;
            var bodyColor = PdfSharp.Drawing.XBrushes.MidnightBlue;
            var footerColor = PdfSharp.Drawing.XBrushes.SteelBlue;

            // Define some formatting
            double lineSpacing = 30;
            double currentY = 20;

            // Add header to the PDF
            string header = "**********************************************************\n*                                                          *\n*    Ruhuna Health Care System Patient Record            *\n*                                                          *\n**********************************************************";
            foreach (var line in header.Split('\n'))
            {
                gfx.DrawString(line, headerFont, headerColor, new PdfSharp.Drawing.XRect(40, currentY, page.Width - 80, 0));
                currentY += lineSpacing;
            }

            // Add content from the text fields
            string body = $"Patient ID: {SelectedPatientId}\n" +
                          $"First Name: {txtFirstName.Text}\n" +
                          $"Last Name: {txtLastName.Text}\n" +
                          $"Date of Birth: {txtDob.Value.ToShortDateString()}\n" +
                          $"Gender: {txtGender.SelectedItem.ToString()}\n" +
                          $"Contact Number: {txtContactNum.Text}\n" +
                          $"Address: {txtAddress.Text}\n" +
                          $"Medical History: {txtMedHistory.Text}\n" +
                          $"Registered Date: {DateTime.Now.ToShortDateString()}";

            foreach (var line in body.Split('\n'))
            {
                if (line.StartsWith("Patient ID:") || line.StartsWith("First Name:") || line.StartsWith("Last Name:") || line.StartsWith("Date of Birth:") || line.StartsWith("Gender:") || line.StartsWith("Contact Number:") || line.StartsWith("Address:") || line.StartsWith("Medical History:") || line.StartsWith("Registered Date:"))
                {
                    gfx.DrawString(line, bodyFontBold, bodyColor, new PdfSharp.Drawing.XRect(40, currentY, page.Width - 80, 0));
                }
                else
                {
                    gfx.DrawString(line, bodyFont, bodyColor, new PdfSharp.Drawing.XRect(40, currentY, page.Width - 80, 0));
                }
                currentY += lineSpacing;
            }

            // Add footer to the PDF
            string footer = "**********************************************************";
            gfx.DrawString(footer, footerFont, footerColor, new PdfSharp.Drawing.XRect(40, currentY, page.Width - 80, 0));

            // Save the document
            document.Save("D:\\AD Projects\\pdf\\Patient.pdf");

            MessageBox.Show("PDF Generated Successfully!");
        }

    }
}
