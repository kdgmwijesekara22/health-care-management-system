﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace HealthCare_Plus__HMS.Admin
{
    public partial class Doctors : Form
    {
        public Doctors()
        {
            InitializeComponent();
            loadTblDoctor();
            tblDoctor.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(DoctorDGV_DataBindingComplete);
            LoadDoctorNames();


        }
        SqlConnection Con = new SqlConnection(@"Data Source=GEETH_MALINDA\SQLEXPRESS;Initial Catalog=HealthCarePlus;Integrated Security=True");

        //load doctor combo box 
        private void LoadDoctorNames()
        {
            cmbDocName.Items.Clear();  // Clear the combo box items

            try
            {
                Con.Open();  // Open the database connection
                SqlCommand cmd = new SqlCommand("SELECT username FROM Users WHERE role = 'Doctor'", Con);
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    cmbDocName.Items.Add(reader.GetString(0));  // Add doctor usernames to the combo box
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);  // Show any error messages
            }
            finally
            {
                Con.Close();  // Close the database connection
            }
        }

        private void loadTblDoctor()
        {
            try
            {
                Con.Open();

                // Construct the SQL query to retrieve data from the Users and DoctorProfiles tables using an INNER JOIN
                string query = "SELECT u.user_id AS 'Doctor ID', u.username AS 'Doctor Name', " +
                               "u.contact_number AS 'Contact Number', dp.specialization_id AS 'Specialization ID', " +
                               "dp.qualifications AS 'Qualifications', dp.location AS 'Location' " +
                               "FROM Users u " +
                               "INNER JOIN DoctorProfiles dp ON u.user_id = dp.doctor_id";

                // Create a SqlDataAdapter to execute the query and fetch the data
                SqlDataAdapter sda = new SqlDataAdapter(query, Con);

                // Create a DataTable to hold the fetched data
                DataTable dt = new DataTable();

                // Fill the DataTable with the data fetched from the database
                sda.Fill(dt);

                // Bind the DataTable as the data source for the DataGridView (tblDoctor)
                tblDoctor.DataSource = dt;

                // Set the AutoSizeMode property of each column to Fill to make them fill the available space
                foreach (DataGridViewColumn column in tblDoctor.Columns)
                {
                    column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
            catch (Exception ex)
            {
                // Display an error message and additional debugging information if there's an exception while fetching data
                MessageBox.Show("Error: " + ex.Message + "\n\n" + ex.StackTrace);
            }
            finally
            {
                // Close the database connection in the 'finally' block to ensure it's always closed
                Con.Close();
            }
        }


        int Key = 0;
        private void Clear()
        {
            txtDocId.Text = "";
            txtMobnum.Text = "";
            txtEmail.Text = "";
            cmbSpec.SelectedIndex = -1;
            cmbDocName.SelectedIndex = -1;
            txtQulifications.Text = "";
            txtLocation.Text = "";
            // Add code to clear other text fields and combo boxes as needed
            // AdditionalTextField.Text = "";
            // AdditionalComboBox.SelectedIndex = -1;
            Key = 0;
        }

        private void EditBtn_Click(object sender, EventArgs e)
        {
           
        }

        private void DelBtn_Click(object sender, EventArgs e)
        {
          
        }

        private void DoctorDGV_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewColumn column in tblDoctor.Columns)
            {
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            }
        }

        //table row click 
        private void tblDoctorClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0) // Ensure the clicked row index is valid (not a header row)
            {
                DataGridViewRow row = tblDoctor.Rows[e.RowIndex]; // Get the clicked row
                int doctorId = int.Parse(row.Cells["Doctor ID"].Value.ToString());

                try
                {
                    Con.Open();

                    // Fetch the doctor details based on the clicked doctor's ID
                    SqlCommand cmd = new SqlCommand("SELECT u.username, u.contact_number, u.email, dp.specialization_id, dp.qualifications, dp.location FROM Users u INNER JOIN DoctorProfiles dp ON u.user_id = dp.doctor_id WHERE dp.doctor_id = @DoctorId", Con);
                    cmd.Parameters.AddWithValue("@DoctorId", doctorId);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            txtDocId.Text = reader["user_id"].ToString();  // Set the doctor's ID
                            txtMobnum.Text = reader["contact_number"].ToString();  // Set the doctor's mobile number
                            txtEmail.Text = reader["email"].ToString();  // Set the doctor's email
                            cmbDocName.Text = reader["username"].ToString();  // Set the doctor's name
                            cmbSpec.SelectedValue = reader["specialization_id"];  
                            txtQulifications.Text = reader["qualifications"].ToString();
                            txtLocation.Text = reader["location"].ToString();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    if (Con.State == ConnectionState.Open)
                        Con.Close();
                }
            }
        }




        private void RoomNumCb_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Doctors_Load(object sender, EventArgs e)
        {
            LoadSpecializations();
        }

        private void LoadSpecializations()
        {
            try
            {
                // Open the database connection
                Con.Open();

                // Fetch specializations from the database
                string query = "SELECT specialization_name FROM Specializations";
                SqlCommand cmd = new SqlCommand(query, Con);
                SqlDataReader rdr = cmd.ExecuteReader();

                // Clear existing items from the combo box
                cmbSpec.Items.Clear();

                // Add fetched specializations to the combo box
                while (rdr.Read())
                {
                    cmbSpec.Items.Add(rdr["specialization_name"].ToString());
                }

                // Close the reader
                rdr.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                // Close the database connection
                if (Con.State == ConnectionState.Open)
                    Con.Close();
            }
        }

        //Add doctor
        private void btnAdd_Click(object sender, EventArgs e)
        {
            // Check if any of the text fields are empty or if no specialization is selected
            if (string.IsNullOrWhiteSpace(txtDocId.Text) ||
                cmbSpec.SelectedItem == null ||
                string.IsNullOrWhiteSpace(txtQulifications.Text) ||
                string.IsNullOrWhiteSpace(txtLocation.Text))
            {
                MessageBox.Show("Please fill in all the fields!");
                return;  // Exit the method early
            }

            // Validate txtLocation for integer value
            if (!int.TryParse(txtLocation.Text, out int roomNumber))
            {
                MessageBox.Show("Room Number should be a valid integer!");
                return;
            }

            try
            {
                // Fetch the doctor ID from the form (this might be from a text box or combo box)
                int doctorId = int.Parse(txtDocId.Text); 

                // Fetch the selected specialization name from the combo box
                string specializationName = cmbSpec.SelectedItem.ToString();

                // Fetch doctor's qualifications and location from the form
                string qualifications = txtQulifications.Text;
                string location = txtLocation.Text;

                // Open the database connection
                Con.Open();

                // Fetch the specialization ID based on the selected specialization name
                SqlCommand cmdSpec = new SqlCommand("SELECT specialization_id FROM Specializations WHERE specialization_name = @SpecName", Con);
                cmdSpec.Parameters.AddWithValue("@SpecName", specializationName);
                int specializationId = (int)cmdSpec.ExecuteScalar();

                // Insert the new doctor's profile into the DoctorProfiles table
                SqlCommand cmdInsert = new SqlCommand("INSERT INTO DoctorProfiles (doctor_id, specialization_id, qualifications, location) VALUES (@DoctorId, @SpecId, @Qualifications, @Location)", Con);
                cmdInsert.Parameters.AddWithValue("@DoctorId", doctorId);
                cmdInsert.Parameters.AddWithValue("@SpecId", specializationId);
                cmdInsert.Parameters.AddWithValue("@Qualifications", qualifications);
                cmdInsert.Parameters.AddWithValue("@Location", location);
                cmdInsert.ExecuteNonQuery();

                MessageBox.Show("Doctor's profile added successfully!");
                // Call the method to load/refresh the table here
                loadTblDoctor();
                Clear();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Clear();

            }
            finally
            {
                Con.Close();  // Close the database connection
            }
        }


        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtDocId.Text))
            {
                MessageBox.Show("Please select a doctor to edit.");
                return;
            }

            // Check if any of the text fields are empty or if no specialization is selected
            if (string.IsNullOrWhiteSpace(txtDocId.Text) ||
                cmbSpec.SelectedItem == null ||
                string.IsNullOrWhiteSpace(txtQulifications.Text) ||
                string.IsNullOrWhiteSpace(txtLocation.Text))
            {
                MessageBox.Show("Please fill in all the fields!");
                return;  // Exit the method early
            }

            try
            {
                int doctorId = int.Parse(txtDocId.Text);
                string specializationName = cmbSpec.SelectedItem.ToString();
                string qualifications = txtQulifications.Text;
                int location = int.Parse(txtLocation.Text);  // Assuming location is a numeric field

                if (Con.State == ConnectionState.Closed)
                {
                    Con.Open();
                }

                // Fetch the specialization ID based on the selected specialization name
                SqlCommand cmdSpec = new SqlCommand("SELECT specialization_id FROM Specializations WHERE specialization_name = @SpecName", Con);
                cmdSpec.Parameters.AddWithValue("@SpecName", specializationName);
                int specializationId = (int)cmdSpec.ExecuteScalar();

                // Update the doctor's profile in the DoctorProfiles table
                SqlCommand cmdUpdate = new SqlCommand("UPDATE DoctorProfiles SET specialization_id = @SpecId, qualifications = @Qualifications, location = @Location WHERE doctor_id = @DoctorId", Con);
                cmdUpdate.Parameters.AddWithValue("@DoctorId", doctorId);
                cmdUpdate.Parameters.AddWithValue("@SpecId", specializationId);
                cmdUpdate.Parameters.AddWithValue("@Qualifications", qualifications);
                cmdUpdate.Parameters.AddWithValue("@Location", location);
                cmdUpdate.ExecuteNonQuery();

                MessageBox.Show("Doctor's profile updated successfully!");
                loadTblDoctor();  // Refresh the table to show the updated details
                Clear();  // Clear the input fields

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (Con.State == ConnectionState.Open)
                    Con.Close();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtDocId.Text))
            {
                MessageBox.Show("Please select a doctor to delete.");
                return;
            }

            try
            {
                int doctorId = int.Parse(txtDocId.Text);

                Con.Open();

                // Delete the doctor's profile from the DoctorProfiles table
                SqlCommand cmdDelete = new SqlCommand("DELETE FROM DoctorProfiles WHERE doctor_id = @DoctorId", Con);
                cmdDelete.Parameters.AddWithValue("@DoctorId", doctorId);
                cmdDelete.ExecuteNonQuery();

                MessageBox.Show("Doctor's profile deleted successfully!");
                loadTblDoctor();  // Refresh the table
                Clear();  // Clear the input fields

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (Con.State == ConnectionState.Open)
                    Con.Close();
            }
        }

        private void cmbDocName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbDocName.SelectedItem == null)
                return;  // Exit early if no item is selected

            // Fetch selected doctor's name
            string selectedDoctorName = cmbDocName.SelectedItem.ToString();


            try
            {
                if (Con.State == ConnectionState.Closed)
                {
                    Con.Open();  // Only open if it's closed
                }
                SqlCommand cmd = new SqlCommand("SELECT user_id, contact_number, email FROM Users WHERE username = @DoctorName", Con);
                cmd.Parameters.AddWithValue("@DoctorName", selectedDoctorName);
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    txtDocId.Text = reader["user_id"].ToString();      // Set the doctor's ID
                    txtMobnum.Text = reader["contact_number"].ToString();  // Set the doctor's mobile number
                    txtEmail.Text = reader["email"].ToString();      // Set the doctor's email
                    
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);  // Show any error messages
            }
            finally
            {
                if (Con.State == ConnectionState.Open)
                    Con.Close();  // Close the database connection
            }
        }

        private void txtDocId_TextChanged(object sender, EventArgs e)
        {


        }

        private void txtMobnum_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtEmail_TextChanged(object sender, EventArgs e)
        {

        }

        private void cmbSpec_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtQulifications_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtLocation_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
