﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HealthCare_Plus__HMS.Admin
{
    public partial class Rooms : Form
    {
        public Rooms()
        {
            InitializeComponent();
            ClearAllFields();  // Clears all fields
            LoadRooms();

        }

        SqlConnection Con = new SqlConnection(@"Data Source=GEETH_MALINDA\SQLEXPRESS;Initial Catalog=HealthCarePlus;Integrated Security=True");

        int Key = 0;
        private void ClearAllFields()
        {
            txtRoomNo.Text = "";
            cmbFloor.SelectedIndex = -1;
            cmbRoomType.SelectedIndex = -1;

            cmbStatus.SelectedIndex = -1;
            txtNote.Text = "";
            Key = 0;
        }

        private void LoadRooms()
        {
            try
            {
                if (Con.State == ConnectionState.Closed)
                    Con.Open();

                string Query = "Select room_id, room_number, floor, room_type, status, notes from Rooms";
                SqlDataAdapter sda = new SqlDataAdapter(Query, Con);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                tbl.DataSource = dt;
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                if (Con.State == ConnectionState.Open)
                    Con.Close();
            }
        }




        private void tblRowClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = tbl.Rows[e.RowIndex];
            Key = Convert.ToInt32(row.Cells[0].Value);
            txtRoomNo.Text = row.Cells[1].Value.ToString();
            cmbFloor.Text = row.Cells[2].Value.ToString();
            cmbRoomType.Text = row.Cells[3].Value.ToString();
            cmbStatus.Text = row.Cells[4].Value.ToString();
            txtNote.Text = row.Cells[5].Value.ToString();
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void txtRoomNo_TextChanged(object sender, EventArgs e)
        {

        }

        private void cmbFloor_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbRoomType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtNote_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            // Ensure a room is selected before deletion
            if (Key == 0)
            {
                MessageBox.Show("Please Select a Room to Delete");
                return;
            }
            try
            {
                // Ensure connection is open
                if (Con.State == ConnectionState.Closed)
                    Con.Open();

                // SQL command to delete the selected room
                SqlCommand cmd = new SqlCommand("DELETE FROM Rooms WHERE room_id = @RoomId", Con);
                cmd.Parameters.AddWithValue("@RoomId", Key);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Room Deleted Successfully");
                LoadRooms();
                ClearAllFields();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                // Ensure connection is closed
                if (Con.State == ConnectionState.Open)
                    Con.Close();
            }
        }



        private void btnEdit_Click(object sender, EventArgs e)
        {
            // Basic validation: Ensure room number is not empty
            if (string.IsNullOrWhiteSpace(txtRoomNo.Text))
            {
                MessageBox.Show("Please enter a room number.");
                return;
            }

            try
            {
                // Ensure connection is open
                if (Con.State == ConnectionState.Closed)
                    Con.Open();

                // SQL command to update the selected room's details
                SqlCommand cmd = new SqlCommand("UPDATE Rooms SET room_number=@RoomNo, floor=@Floor, room_type=@RoomType, status=@Status, notes=@Notes WHERE room_id=@RoomId", Con);
                cmd.Parameters.AddWithValue("@RoomNo", txtRoomNo.Text);
                cmd.Parameters.AddWithValue("@Floor", cmbFloor.SelectedItem.ToString());
                cmd.Parameters.AddWithValue("@RoomType", cmbRoomType.SelectedItem.ToString());
                cmd.Parameters.AddWithValue("@Status", cmbStatus.SelectedItem.ToString());
                cmd.Parameters.AddWithValue("@Notes", txtNote.Text);
                cmd.Parameters.AddWithValue("@RoomId", Key);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Room Updated Successfully");
                LoadRooms();
                ClearAllFields();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                // Ensure connection is closed
                if (Con.State == ConnectionState.Open)
                    Con.Close();
            }
        }



        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtRoomNo.Text) ||
                cmbFloor.SelectedItem == null ||
                cmbRoomType.SelectedItem == null ||
                cmbStatus.SelectedItem == null)
            {
                MessageBox.Show("Please fill out all fields.");
                return;
            }

            try
            {
                if (Con.State == ConnectionState.Closed)
                    Con.Open();

                SqlCommand cmd = new SqlCommand("INSERT INTO Rooms(room_number, floor, room_type, status, notes) VALUES(@RoomNo, @Floor, @RoomType, @Status, @Notes)", Con);
                cmd.Parameters.AddWithValue("@RoomNo", txtRoomNo.Text);
                cmd.Parameters.AddWithValue("@Floor", cmbFloor.SelectedItem.ToString());
                cmd.Parameters.AddWithValue("@RoomType", cmbRoomType.SelectedItem.ToString());
                cmd.Parameters.AddWithValue("@Status", cmbStatus.SelectedItem.ToString());
                cmd.Parameters.AddWithValue("@Notes", txtNote.Text);
                cmd.ExecuteNonQuery();

                MessageBox.Show("Room Added Successfully");
                LoadRooms();
                ClearAllFields();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                if (Con.State == ConnectionState.Open)
                    Con.Close();
            }
        }

    }
}
