﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HealthCare_Plus__HMS.Admin
{
    public partial class Staff : Form
    {
        public Staff()
        {
            InitializeComponent();
            loadTbl();
        }

        SqlConnection Con = new SqlConnection(@"Data Source=GEETH_MALINDA\SQLEXPRESS;Initial Catalog=HealthCarePlus;Integrated Security=True");
        int Key = 0;

        private void loadTbl()
        {
            try
            {
                Con.Open();
                string Query = "Select user_id, username, password, role, email, contact_number, date_created, last_login from Users";
                SqlDataAdapter sda = new SqlDataAdapter(Query, Con);
                SqlCommandBuilder builder = new SqlCommandBuilder(sda);
                var ds = new DataSet();
                sda.Fill(ds);
                tblStaff.DataSource = ds.Tables[0];
                tblStaff.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                Con.Close();
            }
        }

        private void Clear()
        {
            txtName.Text = "";
            txtContactNum.Text = "";
            txtPassward.Text = "";
            txtEmail.Text = "";
            Key = 0;
        }
        //Save Staff Member 
        private void btnAddClick(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtName.Text) ||
                string.IsNullOrWhiteSpace(txtContactNum.Text) ||
                string.IsNullOrWhiteSpace(txtPassward.Text) ||
                string.IsNullOrWhiteSpace(txtEmail.Text))
            {
                MessageBox.Show("Missing Information");
                return;
            }

            // Email validation
            try
            {
                var addr = new System.Net.Mail.MailAddress(txtEmail.Text);
            }
            catch
            {
                MessageBox.Show("Invalid Email Address");
                return;
            }

            // Contact number validation
            if (!System.Text.RegularExpressions.Regex.IsMatch(txtContactNum.Text, @"^\d+$"))
            {
                MessageBox.Show("Invalid Contact Number. Only numeric values are allowed.");
                return;
            }

            // Password validation
            if (!System.Text.RegularExpressions.Regex.IsMatch(txtPassward.Text, @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$"))
            {
                MessageBox.Show("Password must contain at least one uppercase letter, one lowercase letter, and one number.");
                return;
            }

            try
            {
                Con.Open();
                SqlCommand cmd = new SqlCommand("INSERT INTO Users(username, password, role, email, contact_number) VALUES(@SN, @SPA, @SR, @SE, @SP)", Con);
                cmd.Parameters.AddWithValue("@SN", txtName.Text);
                cmd.Parameters.AddWithValue("@SPA", txtPassward.Text); // This should ideally be hashed and salted
                cmd.Parameters.AddWithValue("@SR", roleCb.SelectedItem.ToString());
                cmd.Parameters.AddWithValue("@SE", txtEmail.Text);
                cmd.Parameters.AddWithValue("@SP", txtContactNum.Text);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Staff Member Added");
                loadTbl();
                Clear();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                Con.Close();
            }
        }

        // Edit an existing staff member's details in the Users table
        private void EditBtn_Click(object sender, EventArgs e)
        {
            // Check if the essential fields are filled
            if (string.IsNullOrWhiteSpace(txtName.Text) ||
                string.IsNullOrWhiteSpace(txtContactNum.Text) ||
                string.IsNullOrWhiteSpace(txtPassward.Text) ||
                string.IsNullOrWhiteSpace(txtEmail.Text)) // Added email validation
            {
                MessageBox.Show("Missing Information");
                return;
            }

            // Email validation
            try
            {
                var addr = new System.Net.Mail.MailAddress(txtEmail.Text);
            }
            catch
            {
                MessageBox.Show("Invalid Email Address");
                return;
            }

            // Contact number validation
            if (!System.Text.RegularExpressions.Regex.IsMatch(txtContactNum.Text, @"^\d+$"))
            {
                MessageBox.Show("Invalid Contact Number. Only numeric values are allowed.");
                return;
            }

            // Password validation
            if (!System.Text.RegularExpressions.Regex.IsMatch(txtPassward.Text, @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$"))
            {
                MessageBox.Show("Password must contain at least one uppercase letter, one lowercase letter, and one number.");
                return;
            }

            try
            {
                // Open database connection
                Con.Open();

                // Prepare SQL command to update the details of the selected user
                SqlCommand cmd = new SqlCommand("UPDATE Users SET username=@SN, contact_number=@SP, password=@SPA, email=@SE, role=@SR WHERE user_id=@SKey", Con);
                cmd.Parameters.AddWithValue("@SN", txtName.Text);
                cmd.Parameters.AddWithValue("@SP", txtContactNum.Text);
                cmd.Parameters.AddWithValue("@SPA", txtPassward.Text);
                cmd.Parameters.AddWithValue("@SE", txtEmail.Text);
                cmd.Parameters.AddWithValue("@SR", roleCb.SelectedItem.ToString());
                cmd.Parameters.AddWithValue("@SKey", Key);

                // Execute the SQL command
                cmd.ExecuteNonQuery();
                MessageBox.Show("Staff Member Updated");
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                // Close database connection, refresh the table view and clear the input fields
                Con.Close();
                loadTbl();
                Clear();
            }
        }


        // Delete a staff member from the Users table based on the selected ID (Key)
        private void DelBtn_Click(object sender, EventArgs e)
        {
            // Check if a staff member is selected
            if (Key == 0)
            {
                MessageBox.Show("Select the Staff Member");
                return;
            }

            try
            {
                // Open database connection
                Con.Open();

                // Prepare SQL command to delete the selected user
                SqlCommand cmd = new SqlCommand("DELETE FROM Users WHERE user_id = @SKey", Con);
                cmd.Parameters.AddWithValue("@SKey", Key);

                // Execute the SQL command
                cmd.ExecuteNonQuery();
                MessageBox.Show("Staff Member Deleted");
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
            finally
            {
                // Close database connection, refresh the table view and clear the input fields
                Con.Close();
                loadTbl();
                Clear();
            }
        }


        private void tblStaffRowClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;  // Exit if the row index is not valid

            DataGridViewRow row = tblStaff.Rows[e.RowIndex];

            // Safely retrieve cell values using a helper function
            txtName.Text = GetCellValue(row, "username");
            txtContactNum.Text = GetCellValue(row, "contact_number");
            txtPassward.Text = GetCellValue(row, "password");
            txtEmail.Text = GetCellValue(row, "email");
            roleCb.Text = GetCellValue(row, "role");

            // Use the helper function to convert the StaffId (or user_id) to an integer
            Key = ConvertToInt(GetCellValue(row, "user_id"));
        }

        // Helper function to safely retrieve cell values
        private string GetCellValue(DataGridViewRow row, string columnName)
        {
            return row.Cells[columnName]?.Value?.ToString() ?? "";
        }

        // Helper function to safely convert strings to integers
        private int ConvertToInt(string value)
        {
            int.TryParse(value, out int result);
            return result;
        }


        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtContactNum_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPassward_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtEmail_TextChanged(object sender, EventArgs e)
        {

        }

        private void roleCb_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Staff_Load(object sender, EventArgs e)
        {

        }
    }
}
