﻿namespace HealthCare_Plus__HMS.Admin
{
    partial class AdminDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminDashboard));
            this.panelShadow = new System.Windows.Forms.Panel();
            this.iconPictureBoxClose = new FontAwesome.Sharp.IconPictureBox();
            this.iconPictureBoxMin = new FontAwesome.Sharp.IconPictureBox();
            this.lblTitleChildForm = new System.Windows.Forms.Label();
            this.panelTitleBar = new System.Windows.Forms.Panel();
            this.btnHome = new System.Windows.Forms.PictureBox();
            this.btnRooms = new FontAwesome.Sharp.IconButton();
            this.btnPrescription = new FontAwesome.Sharp.IconButton();
            this.btnStaff = new FontAwesome.Sharp.IconButton();
            this.btnPatient = new FontAwesome.Sharp.IconButton();
            this.btnDoctors = new FontAwesome.Sharp.IconButton();
            this.btnDashboard = new FontAwesome.Sharp.IconButton();
            this.panelDesktop = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.HIVlbl = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Prescriptionlbl = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.StaffNumlbl = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.PatNumlbl = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.DocNumlbl = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.btnLaboratoryTest = new FontAwesome.Sharp.IconButton();
            this.btnLogout = new FontAwesome.Sharp.IconButton();
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBoxClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBoxMin)).BeginInit();
            this.panelTitleBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnHome)).BeginInit();
            this.panelDesktop.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel7.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.panelMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelShadow
            // 
            this.panelShadow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(144)))), ((int)(((byte)(234)))));
            this.panelShadow.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelShadow.Location = new System.Drawing.Point(278, 79);
            this.panelShadow.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panelShadow.Name = "panelShadow";
            this.panelShadow.Size = new System.Drawing.Size(924, 8);
            this.panelShadow.TabIndex = 6;
            // 
            // iconPictureBoxClose
            // 
            this.iconPictureBoxClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconPictureBoxClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(144)))), ((int)(((byte)(234)))));
            this.iconPictureBoxClose.ForeColor = System.Drawing.Color.Gainsboro;
            this.iconPictureBoxClose.IconChar = FontAwesome.Sharp.IconChar.X;
            this.iconPictureBoxClose.IconColor = System.Drawing.Color.Gainsboro;
            this.iconPictureBoxClose.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iconPictureBoxClose.IconSize = 25;
            this.iconPictureBoxClose.Location = new System.Drawing.Point(1162, 18);
            this.iconPictureBoxClose.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.iconPictureBoxClose.Name = "iconPictureBoxClose";
            this.iconPictureBoxClose.Size = new System.Drawing.Size(25, 29);
            this.iconPictureBoxClose.TabIndex = 4;
            this.iconPictureBoxClose.TabStop = false;
            this.iconPictureBoxClose.Click += new System.EventHandler(this.iconPictureBoxClose_Click);
            // 
            // iconPictureBoxMin
            // 
            this.iconPictureBoxMin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconPictureBoxMin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(144)))), ((int)(((byte)(234)))));
            this.iconPictureBoxMin.ForeColor = System.Drawing.Color.Gainsboro;
            this.iconPictureBoxMin.IconChar = FontAwesome.Sharp.IconChar.Minus;
            this.iconPictureBoxMin.IconColor = System.Drawing.Color.Gainsboro;
            this.iconPictureBoxMin.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iconPictureBoxMin.IconSize = 25;
            this.iconPictureBoxMin.Location = new System.Drawing.Point(1124, 18);
            this.iconPictureBoxMin.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.iconPictureBoxMin.Name = "iconPictureBoxMin";
            this.iconPictureBoxMin.Size = new System.Drawing.Size(25, 29);
            this.iconPictureBoxMin.TabIndex = 2;
            this.iconPictureBoxMin.TabStop = false;
            this.iconPictureBoxMin.Click += new System.EventHandler(this.iconPictureBoxMin_Click);
            // 
            // lblTitleChildForm
            // 
            this.lblTitleChildForm.AutoSize = true;
            this.lblTitleChildForm.Font = new System.Drawing.Font("Century Gothic", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitleChildForm.ForeColor = System.Drawing.Color.Gainsboro;
            this.lblTitleChildForm.Location = new System.Drawing.Point(85, 22);
            this.lblTitleChildForm.Name = "lblTitleChildForm";
            this.lblTitleChildForm.Size = new System.Drawing.Size(229, 33);
            this.lblTitleChildForm.TabIndex = 1;
            this.lblTitleChildForm.Text = "Health Care Plus";
            // 
            // panelTitleBar
            // 
            this.panelTitleBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(144)))), ((int)(((byte)(234)))));
            this.panelTitleBar.Controls.Add(this.btnHome);
            this.panelTitleBar.Controls.Add(this.iconPictureBoxClose);
            this.panelTitleBar.Controls.Add(this.iconPictureBoxMin);
            this.panelTitleBar.Controls.Add(this.lblTitleChildForm);
            this.panelTitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitleBar.Location = new System.Drawing.Point(0, 0);
            this.panelTitleBar.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panelTitleBar.Name = "panelTitleBar";
            this.panelTitleBar.Size = new System.Drawing.Size(1202, 79);
            this.panelTitleBar.TabIndex = 5;
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(144)))), ((int)(((byte)(234)))));
            this.btnHome.Image = ((System.Drawing.Image)(resources.GetObject("btnHome.Image")));
            this.btnHome.Location = new System.Drawing.Point(24, 18);
            this.btnHome.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(46, 44);
            this.btnHome.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnHome.TabIndex = 0;
            this.btnHome.TabStop = false;
            // 
            // btnRooms
            // 
            this.btnRooms.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(144)))), ((int)(((byte)(234)))));
            this.btnRooms.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRooms.FlatAppearance.BorderSize = 0;
            this.btnRooms.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRooms.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnRooms.IconChar = FontAwesome.Sharp.IconChar.Bed;
            this.btnRooms.IconColor = System.Drawing.Color.Gainsboro;
            this.btnRooms.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnRooms.IconSize = 40;
            this.btnRooms.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRooms.Location = new System.Drawing.Point(0, 315);
            this.btnRooms.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnRooms.Name = "btnRooms";
            this.btnRooms.Padding = new System.Windows.Forms.Padding(12, 0, 25, 0);
            this.btnRooms.Size = new System.Drawing.Size(278, 63);
            this.btnRooms.TabIndex = 6;
            this.btnRooms.Text = "Rooms";
            this.btnRooms.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRooms.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRooms.UseVisualStyleBackColor = false;
            this.btnRooms.Click += new System.EventHandler(this.btnRooms_Click);
            // 
            // btnPrescription
            // 
            this.btnPrescription.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(144)))), ((int)(((byte)(234)))));
            this.btnPrescription.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPrescription.FlatAppearance.BorderSize = 0;
            this.btnPrescription.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrescription.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnPrescription.IconChar = FontAwesome.Sharp.IconChar.Medkit;
            this.btnPrescription.IconColor = System.Drawing.Color.Gainsboro;
            this.btnPrescription.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnPrescription.IconSize = 40;
            this.btnPrescription.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrescription.Location = new System.Drawing.Point(0, 252);
            this.btnPrescription.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnPrescription.Name = "btnPrescription";
            this.btnPrescription.Padding = new System.Windows.Forms.Padding(12, 0, 25, 0);
            this.btnPrescription.Size = new System.Drawing.Size(278, 63);
            this.btnPrescription.TabIndex = 5;
            this.btnPrescription.Text = "View Prescription";
            this.btnPrescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrescription.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPrescription.UseVisualStyleBackColor = false;
            this.btnPrescription.Click += new System.EventHandler(this.btnPrescription_Click);
            // 
            // btnStaff
            // 
            this.btnStaff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(144)))), ((int)(((byte)(234)))));
            this.btnStaff.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnStaff.FlatAppearance.BorderSize = 0;
            this.btnStaff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStaff.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnStaff.IconChar = FontAwesome.Sharp.IconChar.UserGroup;
            this.btnStaff.IconColor = System.Drawing.Color.Gainsboro;
            this.btnStaff.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnStaff.IconSize = 40;
            this.btnStaff.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStaff.Location = new System.Drawing.Point(0, 189);
            this.btnStaff.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnStaff.Name = "btnStaff";
            this.btnStaff.Padding = new System.Windows.Forms.Padding(12, 0, 25, 0);
            this.btnStaff.Size = new System.Drawing.Size(278, 63);
            this.btnStaff.TabIndex = 4;
            this.btnStaff.Text = "Staff";
            this.btnStaff.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStaff.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnStaff.UseVisualStyleBackColor = false;
            this.btnStaff.Click += new System.EventHandler(this.btnStaff_Click);
            // 
            // btnPatient
            // 
            this.btnPatient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(144)))), ((int)(((byte)(234)))));
            this.btnPatient.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPatient.FlatAppearance.BorderSize = 0;
            this.btnPatient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPatient.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnPatient.IconChar = FontAwesome.Sharp.IconChar.UserAlt;
            this.btnPatient.IconColor = System.Drawing.Color.Gainsboro;
            this.btnPatient.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnPatient.IconSize = 40;
            this.btnPatient.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPatient.Location = new System.Drawing.Point(0, 126);
            this.btnPatient.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnPatient.Name = "btnPatient";
            this.btnPatient.Padding = new System.Windows.Forms.Padding(12, 0, 25, 0);
            this.btnPatient.Size = new System.Drawing.Size(278, 63);
            this.btnPatient.TabIndex = 3;
            this.btnPatient.Text = "View Patients";
            this.btnPatient.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPatient.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPatient.UseVisualStyleBackColor = false;
            this.btnPatient.Click += new System.EventHandler(this.btnPatient_Click);
            // 
            // btnDoctors
            // 
            this.btnDoctors.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(144)))), ((int)(((byte)(234)))));
            this.btnDoctors.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnDoctors.FlatAppearance.BorderSize = 0;
            this.btnDoctors.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDoctors.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnDoctors.IconChar = FontAwesome.Sharp.IconChar.UserMd;
            this.btnDoctors.IconColor = System.Drawing.Color.Gainsboro;
            this.btnDoctors.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnDoctors.IconSize = 40;
            this.btnDoctors.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDoctors.Location = new System.Drawing.Point(0, 63);
            this.btnDoctors.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnDoctors.Name = "btnDoctors";
            this.btnDoctors.Padding = new System.Windows.Forms.Padding(12, 0, 25, 0);
            this.btnDoctors.Size = new System.Drawing.Size(278, 63);
            this.btnDoctors.TabIndex = 2;
            this.btnDoctors.Text = "Doctors";
            this.btnDoctors.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDoctors.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDoctors.UseVisualStyleBackColor = false;
            this.btnDoctors.Click += new System.EventHandler(this.btnDoctors_Click);
            // 
            // btnDashboard
            // 
            this.btnDashboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(144)))), ((int)(((byte)(234)))));
            this.btnDashboard.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnDashboard.FlatAppearance.BorderSize = 0;
            this.btnDashboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDashboard.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnDashboard.IconChar = FontAwesome.Sharp.IconChar.ChartColumn;
            this.btnDashboard.IconColor = System.Drawing.Color.Gainsboro;
            this.btnDashboard.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnDashboard.IconSize = 40;
            this.btnDashboard.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDashboard.Location = new System.Drawing.Point(0, 0);
            this.btnDashboard.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnDashboard.Name = "btnDashboard";
            this.btnDashboard.Padding = new System.Windows.Forms.Padding(12, 0, 25, 0);
            this.btnDashboard.Size = new System.Drawing.Size(278, 63);
            this.btnDashboard.TabIndex = 1;
            this.btnDashboard.Text = "Dashboard";
            this.btnDashboard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDashboard.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDashboard.UseVisualStyleBackColor = false;
            this.btnDashboard.Click += new System.EventHandler(this.btnDashboard_Click);
            // 
            // panelDesktop
            // 
            this.panelDesktop.BackColor = System.Drawing.Color.White;
            this.panelDesktop.Controls.Add(this.panel8);
            this.panelDesktop.Controls.Add(this.pictureBox2);
            this.panelDesktop.Controls.Add(this.panel7);
            this.panelDesktop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDesktop.Location = new System.Drawing.Point(278, 87);
            this.panelDesktop.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panelDesktop.Name = "panelDesktop";
            this.panelDesktop.Size = new System.Drawing.Size(924, 582);
            this.panelDesktop.TabIndex = 7;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(144)))), ((int)(((byte)(234)))));
            this.panel8.Controls.Add(this.HIVlbl);
            this.panel8.Controls.Add(this.label9);
            this.panel8.Controls.Add(this.pictureBox6);
            this.panel8.Location = new System.Drawing.Point(662, 101);
            this.panel8.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(218, 142);
            this.panel8.TabIndex = 21;
            // 
            // HIVlbl
            // 
            this.HIVlbl.AutoSize = true;
            this.HIVlbl.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HIVlbl.ForeColor = System.Drawing.Color.White;
            this.HIVlbl.Location = new System.Drawing.Point(90, 98);
            this.HIVlbl.Name = "HIVlbl";
            this.HIVlbl.Size = new System.Drawing.Size(32, 23);
            this.HIVlbl.TabIndex = 13;
            this.HIVlbl.Text = "85";
            this.HIVlbl.Click += new System.EventHandler(this.HIVlbl_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(78, 57);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 26);
            this.label9.TabIndex = 15;
            this.label9.Text = "HIV";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(83, 15);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(46, 40);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 13;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(61, 26);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(547, 374);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 21;
            this.pictureBox2.TabStop = false;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.panel7.Controls.Add(this.panel1);
            this.panel7.Controls.Add(this.panel6);
            this.panel7.Controls.Add(this.panel5);
            this.panel7.Controls.Add(this.panel4);
            this.panel7.Location = new System.Drawing.Point(3, 427);
            this.panel7.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(921, 154);
            this.panel7.TabIndex = 22;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.Prescriptionlbl);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(703, 49);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(197, 78);
            this.panel1.TabIndex = 20;
            // 
            // Prescriptionlbl
            // 
            this.Prescriptionlbl.AutoSize = true;
            this.Prescriptionlbl.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Prescriptionlbl.ForeColor = System.Drawing.Color.Black;
            this.Prescriptionlbl.Location = new System.Drawing.Point(114, 32);
            this.Prescriptionlbl.Name = "Prescriptionlbl";
            this.Prescriptionlbl.Size = new System.Drawing.Size(32, 23);
            this.Prescriptionlbl.TabIndex = 13;
            this.Prescriptionlbl.Text = "50";
            this.Prescriptionlbl.Click += new System.EventHandler(this.Prescriptionlbl_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(79, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 20);
            this.label2.TabIndex = 15;
            this.label2.Text = "Prescriptions";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(15, 19);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(46, 40);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.Controls.Add(this.StaffNumlbl);
            this.panel6.Controls.Add(this.label7);
            this.panel6.Controls.Add(this.pictureBox5);
            this.panel6.Location = new System.Drawing.Point(474, 49);
            this.panel6.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(197, 78);
            this.panel6.TabIndex = 19;
            // 
            // StaffNumlbl
            // 
            this.StaffNumlbl.AutoSize = true;
            this.StaffNumlbl.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StaffNumlbl.ForeColor = System.Drawing.Color.Black;
            this.StaffNumlbl.Location = new System.Drawing.Point(114, 32);
            this.StaffNumlbl.Name = "StaffNumlbl";
            this.StaffNumlbl.Size = new System.Drawing.Size(32, 23);
            this.StaffNumlbl.TabIndex = 13;
            this.StaffNumlbl.Text = "50";
            this.StaffNumlbl.Click += new System.EventHandler(this.StaffNumlbl_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(109, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 20);
            this.label7.TabIndex = 15;
            this.label7.Text = "Staff";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(15, 19);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(46, 40);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 13;
            this.pictureBox5.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.Controls.Add(this.PatNumlbl);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.pictureBox4);
            this.panel5.Location = new System.Drawing.Point(246, 49);
            this.panel5.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(197, 78);
            this.panel5.TabIndex = 19;
            // 
            // PatNumlbl
            // 
            this.PatNumlbl.AutoSize = true;
            this.PatNumlbl.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PatNumlbl.ForeColor = System.Drawing.Color.Black;
            this.PatNumlbl.Location = new System.Drawing.Point(114, 32);
            this.PatNumlbl.Name = "PatNumlbl";
            this.PatNumlbl.Size = new System.Drawing.Size(32, 23);
            this.PatNumlbl.TabIndex = 13;
            this.PatNumlbl.Text = "85";
            this.PatNumlbl.Click += new System.EventHandler(this.PatNumlbl_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(99, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 20);
            this.label3.TabIndex = 15;
            this.label3.Text = "Patients";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(15, 19);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(46, 40);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 13;
            this.pictureBox4.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.DocNumlbl);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.pictureBox8);
            this.panel4.Location = new System.Drawing.Point(16, 49);
            this.panel4.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(197, 78);
            this.panel4.TabIndex = 18;
            // 
            // DocNumlbl
            // 
            this.DocNumlbl.AutoSize = true;
            this.DocNumlbl.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DocNumlbl.ForeColor = System.Drawing.Color.Black;
            this.DocNumlbl.Location = new System.Drawing.Point(114, 32);
            this.DocNumlbl.Name = "DocNumlbl";
            this.DocNumlbl.Size = new System.Drawing.Size(32, 23);
            this.DocNumlbl.TabIndex = 13;
            this.DocNumlbl.Text = "45";
            this.DocNumlbl.Click += new System.EventHandler(this.DocNumlbl_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(98, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 20);
            this.label5.TabIndex = 15;
            this.label5.Text = "Doctors";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(15, 19);
            this.pictureBox8.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(46, 40);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox8.TabIndex = 13;
            this.pictureBox8.TabStop = false;
            // 
            // panelMenu
            // 
            this.panelMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(144)))), ((int)(((byte)(234)))));
            this.panelMenu.Controls.Add(this.btnLaboratoryTest);
            this.panelMenu.Controls.Add(this.btnLogout);
            this.panelMenu.Controls.Add(this.btnRooms);
            this.panelMenu.Controls.Add(this.btnPrescription);
            this.panelMenu.Controls.Add(this.btnStaff);
            this.panelMenu.Controls.Add(this.btnPatient);
            this.panelMenu.Controls.Add(this.btnDoctors);
            this.panelMenu.Controls.Add(this.btnDashboard);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMenu.Location = new System.Drawing.Point(0, 79);
            this.panelMenu.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(278, 590);
            this.panelMenu.TabIndex = 4;
            // 
            // btnLaboratoryTest
            // 
            this.btnLaboratoryTest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(144)))), ((int)(((byte)(234)))));
            this.btnLaboratoryTest.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLaboratoryTest.FlatAppearance.BorderSize = 0;
            this.btnLaboratoryTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLaboratoryTest.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnLaboratoryTest.IconChar = FontAwesome.Sharp.IconChar.Vial;
            this.btnLaboratoryTest.IconColor = System.Drawing.Color.Gainsboro;
            this.btnLaboratoryTest.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnLaboratoryTest.IconSize = 40;
            this.btnLaboratoryTest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLaboratoryTest.Location = new System.Drawing.Point(0, 378);
            this.btnLaboratoryTest.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnLaboratoryTest.Name = "btnLaboratoryTest";
            this.btnLaboratoryTest.Padding = new System.Windows.Forms.Padding(12, 0, 25, 0);
            this.btnLaboratoryTest.Size = new System.Drawing.Size(278, 63);
            this.btnLaboratoryTest.TabIndex = 8;
            this.btnLaboratoryTest.Text = "Laboratory Test";
            this.btnLaboratoryTest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLaboratoryTest.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLaboratoryTest.UseVisualStyleBackColor = false;
            this.btnLaboratoryTest.Click += new System.EventHandler(this.btnLaboratoryTest_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLogout.FlatAppearance.BorderSize = 0;
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnLogout.IconChar = FontAwesome.Sharp.IconChar.SignOutAlt;
            this.btnLogout.IconColor = System.Drawing.Color.Gainsboro;
            this.btnLogout.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnLogout.IconSize = 40;
            this.btnLogout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLogout.Location = new System.Drawing.Point(0, 519);
            this.btnLogout.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Padding = new System.Windows.Forms.Padding(12, 0, 25, 0);
            this.btnLogout.Size = new System.Drawing.Size(278, 63);
            this.btnLogout.TabIndex = 6;
            this.btnLogout.Text = "Logout";
            this.btnLogout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLogout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLogout.UseVisualStyleBackColor = false;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // AdminDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1202, 669);
            this.Controls.Add(this.panelDesktop);
            this.Controls.Add(this.panelShadow);
            this.Controls.Add(this.panelMenu);
            this.Controls.Add(this.panelTitleBar);
            this.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "AdminDashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AdminDashboard";
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBoxClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBoxMin)).EndInit();
            this.panelTitleBar.ResumeLayout(false);
            this.panelTitleBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnHome)).EndInit();
            this.panelDesktop.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.panelMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelShadow;
        private FontAwesome.Sharp.IconPictureBox iconPictureBoxClose;
        private FontAwesome.Sharp.IconPictureBox iconPictureBoxMin;
        private System.Windows.Forms.Label lblTitleChildForm;
        private System.Windows.Forms.Panel panelTitleBar;
        private System.Windows.Forms.PictureBox btnHome;
        private FontAwesome.Sharp.IconButton btnRooms;
        private FontAwesome.Sharp.IconButton btnPrescription;
        private FontAwesome.Sharp.IconButton btnStaff;
        private FontAwesome.Sharp.IconButton btnPatient;
        private FontAwesome.Sharp.IconButton btnDoctors;
        private FontAwesome.Sharp.IconButton btnDashboard;
        private System.Windows.Forms.Panel panelDesktop;
        private System.Windows.Forms.Panel panelMenu;
        private FontAwesome.Sharp.IconButton btnLogout;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label Prescriptionlbl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label StaffNumlbl;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label PatNumlbl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label DocNumlbl;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox8;
        private FontAwesome.Sharp.IconButton btnLaboratoryTest;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label HIVlbl;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox6;
    }
}